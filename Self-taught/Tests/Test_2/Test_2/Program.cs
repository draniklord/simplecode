﻿using System;
//ReSharper disable All

namespace Test_2
{
    public class A
    {
        public string Aa { get; set; }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            A a = new A()
            {
                Aa = "Hi"
            };
            Foo(ref a);

            Console.WriteLine(a.Aa);
        }

        static void Foo(ref A a)
        {
            a = new A()
            {
                Aa = "Hello"
            };
        }
    }
}