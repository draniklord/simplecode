﻿using System;

namespace Test1
{
    public class NotPrimeException : Exception
    {
        private string _value;

        protected NotPrimeException()
        {

        }

        public NotPrimeException(string value) : base(message: String.Format($"{value}. Теперь живи с этим.."))
        {
            _value = value;
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int r = int.Parse(Console.ReadLine());

                if (r > 10)
                {
                    throw new NotPrimeException("Число то больше 10");                  
                }
                else
                {
                    Console.WriteLine(r);
                }
            }
            //catch (Exception ex)
            //{
            //    Console.WriteLine($"Информация об ошибке {ex.StackTrace}");
            //}
            //catch (NotPrimeException)
            //{
            //    Console.WriteLine("Живо все исправил!");
            //}
            finally
            {
                Console.WriteLine("Живо все исправил!");
            }
        }
    }
}