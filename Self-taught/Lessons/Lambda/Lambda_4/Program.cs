﻿using System;

namespace Lambda_4
{
    delegate void MessageHandler(string message);
    delegate int Randomizer(int a, int b);

    internal class Program
    {
        static void Main(string[] args)
        {
            Download("MSDN.com", message =>
            {
                Console.WriteLine($"Delegate message: {message}");
            });
        }

        private static void Download(string Url, MessageHandler onDownloadComplete)
        {
            Console.WriteLine($"Downloading from {Url}...");
            Console.WriteLine($"Downloading from {Url}....");
            Console.WriteLine($"Downloading from {Url}.....");

            onDownloadComplete.Invoke("Download complete");
        }
    }
}