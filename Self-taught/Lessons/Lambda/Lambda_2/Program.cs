﻿using System;

namespace Lambda_2
{
    delegate void MessageHandler(string message);
    delegate int Randomizer(int a, int b);

    internal class Program
    {
        static void Main(string[] args)
        {
            MessageHandler handler = delegate (string message)
            {
                Console.WriteLine(message);
            };

            MessageHandler handler2 = message =>
            {
                Console.WriteLine(message);
            };

            handler("Hi!");
            handler2("Hemlo!");

            Randomizer randomizer = (a, b) => new Random().Next(a, b);
        }
    }
}