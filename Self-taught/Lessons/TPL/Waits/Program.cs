﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Waits
{
    class Program
    {
        // Метод выполняемый в качестве задачи
        static void MyTask()
        {
            Console.WriteLine("MyTask() №{0} запущен", Task.CurrentId);

            for (int count = 0; count < 10; count++)
            {
                Thread.Sleep(500);
                Console.WriteLine("В методе MyTask №{0} подсчет равен {1}", Task.CurrentId, count);
            }

            Console.WriteLine("MyTask() #{0} завершен", Task.CurrentId);
        }

        static void Main()
        {
            Console.WriteLine("Основной поток запущен");

            Task task1 = new Task(MyTask);
            Task task2 = new Task(MyTask);

            // Запустить задачу
            task1.Start();
            task2.Start();

            // Приостановить выполнение метода Main() пока обе задачи не завершатся
            task1.Wait();
            task2.Wait();

            Console.WriteLine("Основной поток завершен");
            Console.ReadLine();
        }
    }
}