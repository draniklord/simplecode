﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace CancelTask_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var cts = new CancellationTokenSource();

            var task = Ex(cts);

            cts.Cancel();
        }

        public static Task Ex (CancellationTokenSource cts)
        {
            return Task.Run(() =>
            {
                for (int i = 0; i < 10; i++)
                {
                    Task.Delay(500).Wait();
                    if (cts.IsCancellationRequested)
                    {
                        cts.Token.ThrowIfCancellationRequested();
                        //return;
                    }
                }
            });
        }
    }
}