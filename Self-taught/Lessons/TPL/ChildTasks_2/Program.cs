﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChildTasks_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var task = Task.Factory.StartNew(() =>
            {
                Console.WriteLine("First task");

                var task2 = Task.Factory.StartNew(() =>
                {
                    Task.Delay(1000).Wait();
                    Console.WriteLine("Second task");
                }, TaskCreationOptions.AttachedToParent);
            });

            task.Wait();
        }
    }
}
