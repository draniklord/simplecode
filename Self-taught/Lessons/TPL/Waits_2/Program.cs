﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Waits_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int j = 3;
            var task = Task<int>.Factory.StartNew(() =>
            {
                int fact = 1;
                for (int i = 1; i <= j; i++)
                {
                    fact = fact * i;
                }

                return fact;
            }).ContinueWith(task1 =>
            {
                Console.WriteLine($"Factorial {j} = {task1.Result}");

            }).ContinueWith(task2 =>
            {

            }).ContinueWith(task3 =>
            {

            });

            task.Wait();
        }
    }
}