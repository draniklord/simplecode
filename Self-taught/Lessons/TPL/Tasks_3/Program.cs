﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Tasks_3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Parallel.Invoke(
                () => { Console.WriteLine($"Hello {Thread.CurrentThread.ManagedThreadId}"); }, 
                () => { Console.WriteLine($"Hi {Thread.CurrentThread.ManagedThreadId}"); });
        }
    }
}
