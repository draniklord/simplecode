﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace TaskResult
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int j = 3;
            Task<int> task = Task<int>.Factory.StartNew( () =>
            {
                int fact = 1;
                for (int i = 1; i <= j; i++)
                {
                    fact = fact * i;
                }

                return fact;
            });

            int result = task.Result;

            Console.WriteLine($"Factorial {j} = {result}");
        }
    }
}