﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace TasksFromResult
{
    internal class Program
    {
        public interface IDo
        {
            Task<int> Task();
        }

        public class MyClass : IDo
        {
            public Task<int> Task()
            {
                //return Task<int>.Factory.StartNew(() => { return 6; }); 
                return Task<int>.FromResult(6);
            }
        }

        private static void Main(string[] args)
        {
            List<Task> tasks = new List<Task>();

            for (int i = 0; i < 10; i++)
            {
                tasks.Add(Task.Factory.StartNew(() =>
                {
                    var r = Task<int>.FromResult(6);

                    Console.WriteLine($"Thread: {Thread.CurrentThread.ManagedThreadId}");
                }));
            }

            Task.WaitAny(tasks.ToArray());

            Console.WriteLine("End");
        }
    }
}