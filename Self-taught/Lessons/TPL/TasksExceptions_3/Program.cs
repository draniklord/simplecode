﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TasksExceptions_3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<Task> tasks = new List<Task>();

            var task = Task.Run(() =>
            {
                Console.WriteLine("Здесь будет исключение");
                throw new Exception();
            });

            try
            {
                for (int i = 0; i < 10; i++)
                {
                    tasks.Add(Task.Factory.StartNew(() =>
                    {
                        throw new Exception();
                    }).ContinueWith( task =>
                    {
                        //task.Exception;
                    }));
                }

                Task.WaitAll(tasks.ToArray());
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}