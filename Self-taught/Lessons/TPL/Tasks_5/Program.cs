﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Tasks_5
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<Task> tasks = new List<Task>();
            List<int> list = new List<int>();
            Random random = new Random();

            for (int i = 0; i < 10; i++)
            {
                list.Add(random.Next(100, 300));
            }

            for (int i = 0; i < list.Count; i++)
            {
                int i1 = i;

                Task task = Task.Factory.StartNew(() =>
                {
                    Console.WriteLine($"Index: {i1} Thread: {Thread.CurrentThread.ManagedThreadId}");
                });

                tasks.Add(task);
            }

            Task.WaitAll(tasks.ToArray());
        }
    }
}