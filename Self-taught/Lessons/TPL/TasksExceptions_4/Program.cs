﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TasksExceptions_4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            TaskScheduler.UnobservedTaskException += TaskScheduler_UnobservedTaskException;

            List<Task> tasks = new List<Task>();

            try
            {
                var task = Task.Run(() =>
                {
                    Console.WriteLine("Здесь будет исключение!");
                    throw new Exception();
                });

                task.Wait();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            Console.ReadKey();
        }

        private static void TaskScheduler_UnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}