﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace WhenAny_1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            List<Task> tasks = new List<Task>();

            for (int i = 0; i < 10; i++)
            {
                tasks.Add(Task.Factory.StartNew(() =>
                {
                    var count = new Random().Next(1000, 3000);

                    Task.Delay(count).Wait();

                    Console.WriteLine($"Thread: {Thread.CurrentThread.ManagedThreadId}");
                    Console.WriteLine($"Itteration counts: {count}");
                }));
            }

            var result = Task.WhenAny(tasks.ToArray());

            //Console.WriteLine(result);
        }
    }
}
