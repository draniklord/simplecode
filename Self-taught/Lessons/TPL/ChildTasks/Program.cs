﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ChildTasks
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var loadUserDataTask = new Task(() =>
            {
                Console.WriteLine("Loading User Data");
                Thread.Sleep(2000);
                string userID = "1234";
                Console.WriteLine("User ID loaded");

                var loadUserPermissionsTask = new Task(() =>
                {
                    Console.WriteLine("Loading User Permissions for user {0}", userID);
                    Thread.Sleep(2000);
                    Console.WriteLine("User permissions loaded");
                }, TaskCreationOptions.AttachedToParent);
                loadUserPermissionsTask.Start();

                var loadUserConfigurationTask = new Task(() =>
                {
                    Console.WriteLine("Loading User Configuration for user {0}", userID);
                    Thread.Sleep(2000);
                    Console.WriteLine("User configuration loaded");
                }, TaskCreationOptions.AttachedToParent);
                loadUserConfigurationTask.Start();

                Console.WriteLine("Parent task finished");
            });

            loadUserDataTask.Start();
            loadUserDataTask.Wait();
            loadUserDataTask.Dispose();
        }
    }
}