﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Parallels
{
    class Program
    {
        // Методы, исполняемые как задача
        static void MyMeth()
        {
            Console.WriteLine("MyMeth запущен");
            for (int count = 0; count < 5; count++)
            {
                Thread.Sleep(500);
                Console.WriteLine("--> MyMeth Count=" + count);
            }
            Console.WriteLine("MyMeth завершен");
        }

        static void MyMeth2()
        {
            Console.WriteLine("MyMeth2 запущен");
            for (int count = 0; count < 5; count++)
            {
                Thread.Sleep(500);
                Console.WriteLine("--> MyMeth2 Count=" + count);
            }
            Console.WriteLine("MyMeth2 завершен");
        }

        static void Main()
        {
            Console.WriteLine("Основной поток запущен");

            // Выполнить параллельно оба именованных метода
            Parallel.Invoke(MyMeth, MyMeth2);

            Console.WriteLine("Основной поток завершен");
        }
    }
}