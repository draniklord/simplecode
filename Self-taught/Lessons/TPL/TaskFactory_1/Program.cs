﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace TaskFactory_1
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Основной поток запущен");

            // Используем лямбда-выражение
            Task task1 = Task.Factory.StartNew(() => {
                Console.WriteLine("MyTask() №{0} запущен", Task.CurrentId);

                for (int count = 0; count < 10; count++)
                {
                    Thread.Sleep(500);
                    Console.WriteLine("В методе MyTask №{0} подсчет равен {1}", Task.CurrentId, count);
                }

                Console.WriteLine("MyTask() #{0} завершен", Task.CurrentId);
            });

            task1.Wait();
            task1.Dispose();

            Console.WriteLine("Основной поток завершен");
            Console.ReadLine();
        }
    }
}