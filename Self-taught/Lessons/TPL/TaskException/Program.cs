﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace TaskException
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int j = 3;
            var task = Task<int>.Factory.StartNew(() =>
            {
                int fact = 1;
                for (int i = 1; i <= j; i++)
                {
                    fact = fact * i;
                }

                return fact;
            }).ContinueWith(task1 =>
            {
                if (task1.IsCompleted)
                {
                    Console.WriteLine($"Factorial {j} = {task1.Result}");
                }
                else
                {
                    var ex = task1.Exception;
                    Console.WriteLine(ex);
                }
            }).ContinueWith(task2 =>
            {

            }).ContinueWith(task3 =>
            {

            });

            task.Wait();
        }
    }
}