﻿using System;

namespace Algorithm_1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int x = 10;
            if (x > 0)
            {
                x--;
            }
            else
            {
                x++;
            }

            Console.WriteLine(x);

            Console.WriteLine();

            int key = 10;
            int[] nambers = new int[15];

            int result1 = Rank(key, nambers);
            Console.WriteLine(result1);

            Console.WriteLine();

            int result2 = Factorial(3);
            Console.WriteLine(result2);

            Console.WriteLine();

            BubbleSort(nambers);

            Console.WriteLine();

            char[] chars = new char[] { 'A', 'B', 'C' };
            for (int i = 0; i < chars.Length; i++)
            {
                for (int j = 0; j < chars.Length; j++)
                {
                    for (int k = 0; k < chars.Length; k++)
                    {
                        Console.WriteLine($"{chars[i]}{chars[j]}{chars[k]}");
                    }
                }
            }

            int count = 0;
            int n = 100;
            for (int i = 0; i < n; i++)       
                count++;          
        }

        public static int Rank(int key, int[] numbers)
        {
            int low = 0;
            int high = numbers.Length - 1;
            while (low <= high)
            {
                // находим середину
                int mid = low + (high - low) / 2;
                // если ключ поиска меньше значения в середине
                // то верхней границей будет элемент до середины
                if (key < numbers[mid]) high = mid - 1;
                else if (key > numbers[mid]) low = mid + 1;
                else return mid;
            }
            return -1;
        }

        private static int Factorial(int n)
        {
            int result = 1;
            for (int i = 1; i <= n; i++)
            {
                result *= i;
            }
            return result;
        }

        private static void BubbleSort(int[] nums)
        {
            int temp;
            for (int i = 0; i < nums.Length - 1; i++)
            {
                for (int j = i + 1; j < nums.Length; j++)
                {
                    if (nums[i] > nums[j])
                    {
                        temp = nums[i];
                        nums[i] = nums[j];
                        nums[j] = temp;
                    }
                }
            }
        }
    }
}