﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Algorithm_2
{
    public class Program<T> where T : IComparable<T>
    {
        public static List<T> MergeSort(List<T> list)
        {
            if (list.Count <= 1)
                return list;

            int middle = list.Count / 2;

            List<T> left = list.Take(middle).ToList();
            List<T> right = list.Skip(middle).ToList();

            left = MergeSort(left);
            right = MergeSort(right);

            return Merge(left, right);
        }

        private static List<T> Merge(List<T> left, List<T> right)
        {
            List<T> result = new List<T>();
            int i = 0, j = 0;

            while (i < left.Count || j < right.Count)
            {
                if (i < left.Count && j < right.Count)
                {
                    if (left[i].CompareTo(right[j]) <= 0)
                        result.Add(left[i++]);
                    else
                        result.Add(right[j++]);
                }
                else if (i < left.Count)
                {
                    result.Add(left[i++]);
                }
                else
                {
                    result.Add(right[i++]);
                }
            }
            return result;
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            
        }
    }
}