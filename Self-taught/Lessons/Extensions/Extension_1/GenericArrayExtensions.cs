﻿namespace Extension_1
{
    public static class GenericArrayExtensions
    {
        public static void Reset<T>(this T[] Array)
        {
            for (int i = 0; i < Array.Length; i++)
            {
                Array[i] = default;
            }
        }
    }
}