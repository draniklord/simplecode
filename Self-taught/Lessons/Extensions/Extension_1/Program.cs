﻿using System;

namespace Extension_1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int[] intArray = new int[10];

            IntArrayExtensions.Randomizer(intArray);

            foreach (var item in intArray)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("=======RESET=======");

            IntArrayExtensions.Reset(intArray, 11);

            foreach (var item in intArray)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("=======RESET=======");

            intArray.Reset();
            intArray.Randomizer();

            foreach (var item in intArray)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            decimal[] decimalArray = new decimal[10];

            Random random = new Random();

            for (int i = 0; i < decimalArray.Length; i++)
            {
                decimalArray[i] = random.Next(1, 100);
            }

            foreach (var item in decimalArray)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("=======RESET=======");

            GenericArrayExtensions.Reset(decimalArray);

            foreach (var item in decimalArray)
            {
                Console.WriteLine(item);
            }
        }
    }
}