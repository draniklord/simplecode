﻿using System;

namespace Partial_1
{
    public partial class TVRemoteControl
    {
        public partial void TurnOnTV()
        {
            TVisOn = true;
            Console.WriteLine("Телевизор включен!");
        }

        public partial void TurnOffTV()
        {
            TVisOn = false;
            Console.WriteLine("Телевизор выключен!");
        }
    }

    public partial class TVRemoteControl
    {

    }

    internal class Program
    {
        static void Main(string[] args)
        {
            TVRemoteControl tVRemoteControl = new TVRemoteControl();
            tVRemoteControl.TurnOnTV();
        }
    }
}