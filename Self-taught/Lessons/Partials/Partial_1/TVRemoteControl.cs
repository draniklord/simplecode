﻿namespace Partial_1
{
    public partial class TVRemoteControl
    {
        public bool TVisOn { get; private set; }

        public partial void TurnOnTV();
        public partial void TurnOffTV();
    }
}