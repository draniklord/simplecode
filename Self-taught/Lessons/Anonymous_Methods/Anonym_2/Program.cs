﻿using System;

namespace Anonym_2
{
    delegate void MessageHandler(string message);

    internal class Program
    {
        static void Main(string[] args)
        {
            MessageHandler handler = delegate (string message)
            {
                Console.WriteLine("Я делегат!1");
            };

            handler += Handler;

            handler("Омагад");           
        }

        // анонимный метод
        private static void Handler(string message)
        {
            Console.WriteLine(message);
        }
    }
}