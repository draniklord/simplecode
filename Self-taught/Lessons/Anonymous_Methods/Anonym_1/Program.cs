﻿using System;

namespace Anonym_1
{
    delegate int Sum(int number);

    class Program
    {
        static Sum SomeVar()
        {
            int result = 0;

            // Вызов анонимного метода
            Sum del = delegate (int number)
            {
                for (int i = 0; i <= number; i++)
                    result += i;
                return result;
            };
            return del;
        }

        static void Main()
        {
            Sum del1 = SomeVar();

            for (int i = 1; i <= 5; i++)
            {
                Console.WriteLine("Cумма {0} равна: {1}", i, del1(i));
            }
        }
    }
}