﻿using System;

namespace BlocksTry
{
    class Program
    {
        static void Main()
        {
            Console.Write("Введите длину массива: ");
            try
            {
                int i = int.Parse(Console.ReadLine());
                int[] myArr = new int[i];
                Console.WriteLine("\nВведите теперь элементы массива: ");
                for (int j = 0; j < i; j++)
                {
                    Console.Write("{0}й элемент: ", j + 1);
                    // Вложенный оператор try
                    try
                    {
                        myArr[j] = int.Parse(Console.ReadLine());
                    }
                    catch (OverflowException)
                    {
                        Console.WriteLine("Слишком большое число");
                    }
                }
            }
            catch (FormatException ex)
            {
                Console.WriteLine("ОШИБКА: " + ex.Message);
            }
        }
    }
}