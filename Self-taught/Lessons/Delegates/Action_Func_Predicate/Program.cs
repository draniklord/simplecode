﻿using System;

namespace Action_Func_Predicate
{
    internal class Program
    {
        static void Main(string[] args)
        {
            DoSmth_1(10, Action);
            DoSmth_2(20, Func);

            Predicate<int> predicate = Predicate;
            var isPositive = predicate.Invoke(10); 
        }

        // Использование Action
        private static void DoSmth_1(int state, Action<int> action)
        {

        }
        private static void Action(int obj)
        {
            Console.WriteLine(obj);
        }

        // Использование Func
        private static void DoSmth_2(int state, Func<int, byte> action)
        {

        }
        private static byte Func(int arg)
        {
            return (byte)arg;
        }

        // Использование Predicate
        private static bool Predicate(int obj)
        {
            if (obj == 0)
                return true;
            return false;
        }
    }
}