﻿using System;

namespace Delegate_2
{
    delegate void MessageHandler(string message);
    delegate int Randomizer(int from, int to);

    internal class Program
    {
        static void Main(string[] args)
        {
            MessageHandler handler = delegate (string message)
            {
                Console.WriteLine(message);
                Console.WriteLine("Я делегат!1");
            };

            handler += Handler;

            handler("Омагад");

            Console.WriteLine();

            Randomizer randomizer = delegate (int from, int to)
            {
                int rand = new Random().Next(from, to);
                Console.WriteLine(rand);
                return rand;
            };

            randomizer += delegate (int from, int to)
            {
                int rand = new Random().Next(from, to);
                Console.WriteLine(rand);
                return rand;
            };

            randomizer(0, 50);
        }

        // анонимный метод
        private static void Handler(string message)
        {
            Console.WriteLine(message);
        }
    }
}