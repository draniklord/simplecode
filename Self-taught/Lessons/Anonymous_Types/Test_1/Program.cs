﻿namespace Test_1
{
    public class Address
    {
        public string address;
        public string city;
        public string state;
        public string postalCode;
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            Address address = new Address
            {
                address = "105 Elm Street",
                city = "Atlanta",
                state = "GA",
                postalCode = "30339"
            };
        }
    }
}