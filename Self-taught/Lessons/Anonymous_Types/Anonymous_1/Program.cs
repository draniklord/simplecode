﻿using System;

namespace Anonymous_1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var address = new
            {
                address = "105 Elm Street",
                city = "Atlanta",
                state = "GA",
                postalCode = "30339"
            };

            Console.WriteLine($"Address = {address.address} : " +
                $"City = {address.city} : " +
                $"State = {address.state} : " +
                $"Zip = {address.postalCode}");
            Console.WriteLine();
            Console.WriteLine(address.GetType().ToString());
        }
    }
}