﻿using System;

namespace Adapter_2
{
    public interface IReportCreator
    {
        Pdf GetPdf();
    }

    public class DocReportCreator
    {
        public Doc GetReport()
        {
            return new Doc();
        }
    }

    public class Doc
    {

    }

    public class Pdf
    {

    }

    public class Adapter : IReportCreator
    {
        private DocReportCreator _docReportCreator;

        public Adapter(DocReportCreator docReportCreator)
        {
            _docReportCreator = docReportCreator;
        }

        public Pdf GetPdf()
        {
            var doc = _docReportCreator.GetReport();
            Console.WriteLine("Формат преобразован");

            return new Pdf();
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {

        }
    }
}