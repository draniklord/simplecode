﻿using System;

namespace Mediator_2
{
    public class Mediator
    {
        private A _a;
        private B _b;

        public void DoA()
        {
            _a.Do();
        }
        public void DoB()
        {
            _b.Do();
        }
    }

    public class A
    {
        private Mediator _mediator;

        public A(Mediator mediator)
        {
            _mediator = mediator;
        }

        public void Do()
        {

        }

        public void DoSmthElse()
        {
            _mediator.DoB();
        }
    }

    public class B
    {
        private Mediator _mediator;

        public B(Mediator mediator)
        {
            _mediator = mediator;
        }

        public void Do()
        {

        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
