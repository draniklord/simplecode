﻿using System;

namespace FactoryMethod_2
{
    public interface ITransport
    {
        public void Delivery(string name);
    }

    public interface ILogostic
    {
        public ITransport GetTransort();
    }

    public class Truck : ITransport
    {
        public void Delivery(string name)
        {
            Console.WriteLine($"Доставка товара {name} на грузовике");
        }
    }

    public class Boat : ITransport
    {
        public void Delivery(string name)
        {
            Console.WriteLine($"Доставка товара {name} на лодке");
        }
    }


    public class TruckLogistic : ILogostic
    {
        public ITransport GetTransort()
        {
            return new Truck();
        }
    }

    public class BoatLogistic : ILogostic
    {
        public ITransport GetTransort()
        {
            return new Boat();
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            ILogostic logostic = new BoatLogistic();
            var transport = logostic.GetTransort();
            transport.Delivery("Пицза");
            transport.Delivery("Бочки");

            logostic = new TruckLogistic();
            transport = logostic.GetTransort();
            transport.Delivery("Бочки");
            transport.Delivery("Пицза");

        }
    }
}