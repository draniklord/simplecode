﻿using System;

namespace Facade_2
{
    public class Facade
    {
        private Auth _auth;
        private Settings _settings;
        private SmsProvider _provider;

        public Facade(Auth auth, Settings settings, SmsProvider provider)
        {
            _auth = auth;
            _settings = settings;
            _provider = provider;
        }

        public void SendSms()
        {
            _settings.DoSmth();
            var token = _auth.DoAuth(_settings);
            _provider.SendSms(token);
        }
    }

    public class Auth
    {
        public string DoAuth(Settings settings)
        {
            return "token";
        }
    }

    public class Settings
    {
        public void DoSmth()
        {

        }
    }

    public class SmsProvider
    {
        public void SendSms(string token)
        {

        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            var facade = new Facade(new Auth(), new Settings(), new SmsProvider());
            facade.SendSms();
        }
    }
}