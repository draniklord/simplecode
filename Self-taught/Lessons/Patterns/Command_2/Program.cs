﻿using System;

namespace Command_2
{
    public interface ICommand
    {
        void Execute();
        void Cancel();
    }

    public class ExitCmd : ICommand
    {
        public void Cancel()
        {
            Console.WriteLine("Отмена!");
        }

        public void Execute()
        {
            Console.WriteLine("Выход!");
        }
    }

    public class Buttons
    {
        private ExitCmd _exitCmd;

        public void BtnExit()
        {
            _exitCmd.Execute();
        }
    }

    public class ShortKeys
    {
        private ExitCmd _exitCmd;

        public void ShortKeyExit()
        {
            _exitCmd.Execute();
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
