﻿using System;

namespace Singletons
{
    public class Singleton
    {
        private static Singleton instance;

        public static Singleton Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Singleton();  
                }

                return instance;
            }
        }

        public void DoSmthElse()
        {

        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            Singleton.Instance.DoSmthElse();
        }
    }
}