﻿using System;

namespace Reflection_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Type myType = typeof(User);

            Console.WriteLine(myType.ToString());

            User user = new User("Олег", 22);
            user.Display();
            Console.WriteLine(user.Payment(8, 1200));
        }
    }
}