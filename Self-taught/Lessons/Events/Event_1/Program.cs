﻿using System;

namespace Event_1
{
    internal class Program
    {
        delegate void MessageHandler(string message);
        static event MessageHandler Event;

        static void Main(string[] args)
        {
            Event += OnEvent;
            Event += Program_Event;

            Event -= Program_Event;

            Event.Invoke("Message");

            Download("MSDN.com", OnDownloadComplete);
        }

        private static void Program_Event(string message)
        {
            // do smth
        }

        private static void OnEvent(string message)
        {
            // do smth
        }

        private static void OnDownloadComplete(string message)
        {
            Console.WriteLine(message);
        }

        private static void Download(string Url, MessageHandler onDownloadComplete)
        {
            Console.WriteLine($"Downloading from {Url}...");
            Console.WriteLine($"Downloading from {Url}....");
            Console.WriteLine($"Downloading from {Url}.....");

            onDownloadComplete.GetInvocationList();

            onDownloadComplete.Invoke("Download complete");
        }
    }
}