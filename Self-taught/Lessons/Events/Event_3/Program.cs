﻿using System;

namespace Event_3
{
    public delegate void BattaryState(int state);

    public class Battary
    {
        private int state = 100;

        public event BattaryState OnBattaryOff;

        public void Charge()
        {
            state += 10;
            if (state >= 100 || OnBattaryOff != null)
            {
                OnBattaryOff.Invoke(state);
                Console.WriteLine("Батарея заряжена");
            }
        }

        public void Spend()
        {
            state -= 10;

            if (state <= 0 || OnBattaryOff != null)
            {
                OnBattaryOff.Invoke(state);
                Console.WriteLine("Батарея разряжена!");
            }
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            Battary battary = new Battary();

            battary.OnBattaryOff += Battary_OnBattaryOff;

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Батарея разряжается...");
                battary.Spend();
            }
        }

        private static void Battary_OnBattaryOff(int state)
        {
            Console.WriteLine($"Текущее состояние батареи: {state}");
        }
    }
}