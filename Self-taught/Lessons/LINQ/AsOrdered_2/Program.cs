﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AsOrdered_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string[] cars = { "Nissan", "Aston Martin", "Chevrolet", "Alfa Romeo", "Chrysler", "Dodge",
                "BMW", "Ferrari", "Audi", "Bentley", "Ford", "Lexus",
                "Mercedes", "Toyota", "Volvo", "Subaru", "Жигули" };

            // Запрос Parallel LINQ с сохранением порядка
            IEnumerable<string> auto = cars.AsParallel().AsOrdered()
                .Where(p => p.Contains("a"));

            foreach (string s in auto)
                Console.WriteLine(s);
        }
    }
}