﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ToList
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string[] cars = { "Alfa Romeo", "Aston Martin", "Audi", "Nissan", "Chevrolet", "Chrysler",
                "Dodge", "BMW", "Ferrari", "Bentley", "Ford", "Lexus",
                "Mercedes", "Toyota", "Volvo", "Subaru", "Жигули" };

            List<string> auto = cars.ToList();

            foreach (string s in auto)
                Console.Write(s + "  ");
        }
    }
}