﻿using System;
using System.Linq;

namespace Delegates_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = { -2, -1, 0, 1, 2, 3, 4, 5, 6, 7 };

            var result = numbers.Where(i => i > 0).Select(Factorial);

            foreach (int i in result)
                Console.WriteLine(i);
        }

        static int Factorial(int x)
        {
            int result = 1;

            for (int i = 1; i <= x; i++)
                result *= i;

            return result;
        }
    }
}