﻿using System;
using System.Linq;

namespace Delegates
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = { 1, 2, 3, 4, 10, 34, 55, 66, 77, 88 };

            Func<int, bool> MoreThanTen = delegate (int i) { return i > 10; };

            var result = numbers.Where(MoreThanTen);

            foreach (int i in result)
                Console.WriteLine(i);
        }
    }
}