﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Query
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string[] names = {
        "Adams", "Arthur", "Buchanan", "Bush", "Carter", "Cleveland",
        "Clinton", "Coolidge", "Eisenhower", "Fillmore", "Ford",  "Garfield",
        "Grant", "Harding", "Harrison", "Hayes", "Hoover", "Jackson",
        "Jefferson", "Johnson", "Kennedy", "Lincoln", "Madison", "McKinley",
        "Monroe", "Nixon", "Obama", "Pierce", "Polk", "Reagan", "Roosevelt",
        "Taft", "Taylor", "Truman", "Tyler", "Van Buren", "Washington", "Wilson"};

            // Использование точечной нотации
            //IEnumerable<string> sequence = names
            //  .Where(n => n.Length < 6)
            //  .Select(n => n);

            // Использование синтаксиса выражения запроса
            IEnumerable<string> sequence = from n in names
                                           where n.Length < 6
                                           select n;

            foreach (string name in sequence)
            {
                Console.WriteLine("{0}", name);
            }
        }
    }
}