﻿using System;
using System.Linq;

namespace ForAll_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string[] cars = { "Nissan", "Aston Martin", "Chevrolet", "Alfa Romeo", "Chrysler", "Dodge",
                "BMW", "Ferrari", "Audi", "Bentley", "Ford", "Lexus",
                "Mercedes", "Toyota", "Volvo", "Subaru", "Жигули" };

            // Запрос Parallel LINQ
            cars.AsParallel()
                .Where(p => p.Contains("s"))
                .ForAll(p => Console.WriteLine("Название: " + p));
        }
    }
}