﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AggregateException_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string[] cars = { "Nissan", "Aston Martin", "Chevrolet", "Alfa Romeo", "Chrysler", "Dodge",
                "BMW", "Ferrari", "Audi", "Bentley", "Ford", "Lexus",
                "Mercedes", "Toyota", "Volvo", "Subaru", "Жигули" };

            // Последовательный запрос
            IEnumerable<string> auto = cars
                .Select(p =>
                {
                    if (p == "Aston Martin")
                        throw new Exception("Проблемы с машиной " + p);
                    return p;
                });

            try
            {
                foreach (string s in auto)
                    Console.WriteLine("Результат: " + s + "\n");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}