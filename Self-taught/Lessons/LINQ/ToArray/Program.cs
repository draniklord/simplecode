﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ToArray
{
    internal class Program
    {
        static void Main(string[] args)
        {
            IEnumerable<int> item = Enumerable.Range(1, 20);

            Console.WriteLine("Начальный тип: " + item);

            int[] arr = item.ToArray();

            Console.WriteLine("Используем ToArray: " + arr);
        }
    }
}