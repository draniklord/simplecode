﻿using System;
using System.Linq;

namespace ForAll
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = new int[] { -2, -1, 0, 1, 2, 4, 3, 5, 6, 7, 8, };

            numbers
                .AsParallel()
                .Where(n => n > 0)
                .Select(x => Factorial(x))
                .ForAll(i => Console.WriteLine(i));

            //(from n in numbers.AsParallel()
            // where n > 0
            // select Factorial(n))
            // .ForAll(Console.WriteLine);
        }

        static int Factorial(int x)
        {
            int result = 1;

            for (int i = 1; i <= x; i++)
            {
                result *= i;
            }

            return result;
        }
    }
}