﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Select
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Первый прототип Select
            string[] cars = { "Nissan", "Aston Martin", "Chevrolet", "Alfa Romeo", "Chrysler", "Dodge",
                "BMW", "Ferrari", "Audi", "Bentley", "Ford", "Lexus",
                "Mercedes", "Toyota", "Volvo", "Subaru", "Жигули" };

            IEnumerable<int> sequence1 = cars.Select(p => p.Length);

            foreach (int i in sequence1)
                Console.Write(i + " ");

            Console.WriteLine("\n");

            // с использованием лямбда-выражения
            var sequence11 = cars.Select(p => new { p, p.Length });

            foreach (var i in sequence11)
                Console.WriteLine(i);

            Console.WriteLine();

            // Второй прототип Select
            var carObj1 = cars.Select(p => new { LastName = p, Length = p.Length });

            foreach (var i in carObj1)
                Console.WriteLine("Автомобиль {0} имеет длину {1} символов", i.LastName, i.Length);

            Console.WriteLine();

            // 
            var carObj2 = cars.Select((p, i) => new { Index = i + 1, LastName = p });

            foreach (var i in carObj2)
                Console.WriteLine(i.Index + ". " + i.LastName);
        }
    }
}