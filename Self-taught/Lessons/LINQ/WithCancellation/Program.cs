﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace WithCancellation
{
    internal class Program
    {
        static void Main(string[] args)
        {
            CancellationTokenSource cts = new CancellationTokenSource();

            new Task(() =>
            {
                Thread.Sleep(400);
                cts.Cancel();
            }).Start();

            try
            {
                int[] numbers = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, };

                var factorials = from n in numbers.AsParallel().WithCancellation(cts.Token)
                                 select Factorial(n);

                foreach (var n in factorials)
                    Console.WriteLine(n);
            }
            catch (OperationCanceledException ex)
            {
                Console.WriteLine("Операция была прервана");
            }
            catch (AggregateException ex)
            {
                if (ex.InnerExceptions != null)
                {
                    foreach (Exception e in ex.InnerExceptions)
                        Console.WriteLine(e.Message);
                }
            }
            finally
            {
                cts.Dispose();
            }
        }

        static int Factorial(int x)
        {
            int result = 1;

            for (int i = 1; i <= x; i++)
            {
                result *= i;
            }

            Console.WriteLine($"Факториал числа {x} равен {result}");

            Thread.Sleep(1000);

            return result;
        }
    }
}