﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Implementation_3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<User> users = new List<User>();

            users.Add(new User { Id = 3, FirstName = "Timur", LastName = "Ivanov", Age = 22 });
            users.Add(new User { Id = 4, FirstName = "Alex", LastName = "Ivanov", Age = 34 });
            users.Add(new User { Id = 5, FirstName = "Oleg", LastName = "Ivanov", Age = 16 });
            //users.Add(new User { Id = 6, FirstName = "Ivan", LastName = "Ivanov", Age = 56 });

            var whereSelect = users
                .Where(user => user.Age > 18)
                .Select(user => new
                {
                    Name = user.FirstName,
                    Age = user.Age,
                })
                .ToList();

            foreach (var user in whereSelect)
            {
                Console.WriteLine($"{user.Name} {user.Age}");
            }
            Console.WriteLine();

            users.Add(new User { Id = 6, FirstName = "Ivan", LastName = "Ivanov", Age = 56 });

            foreach (var user in whereSelect)
            {
                Console.WriteLine($"{user.Name} {user.Age}");
            }
        }
    }
}