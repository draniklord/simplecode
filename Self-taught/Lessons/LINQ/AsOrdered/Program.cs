﻿using System;
using System.Linq;

namespace AsOrdered
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = new int[] { -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, };

            var factorials = numbers
                .AsParallel()
                .AsOrdered()
                .Where(n => n > 0)
                .Select(x => Factorial(x));
                
            //var factorials = from n in numbers.AsParallel()//.AsOrdered()
            //                 where n > 0
            //                 orderby n
            //                 select Factorial(n);

            foreach (var n in factorials)
                Console.WriteLine(n);
        }

        static int Factorial(int x)
        {
            int result = 1;

            for (int i = 1; i <= x; i++)
            {
                result *= i;
            }

            return result;
        }
    }
}