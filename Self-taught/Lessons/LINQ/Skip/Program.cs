﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Skip
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string[] cars = { "Nissan", "Chevrolet", "Alfa Romeo", "Chrysler", "Dodge", "BMW",
                "Aston Martin", "Ferrari", "Audi", "Bentley", "Ford", "Lexus",
                "Mercedes", "Toyota", "Volvo", "Subaru", "Жигули" };

            IEnumerable<string> auto = cars.Skip(5);

            foreach (string str in auto)
                Console.WriteLine(str);
        }
    }
}