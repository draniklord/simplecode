﻿using System;
using System.Linq;

namespace Implementation_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string[] teams = { "Бавария", "Боруссия", "Реал Мадрид", "Манчестер Сити", "ПСЖ", "Барселона" };

            // определение и выполнение LINQ-запроса
            int i = (from t in teams
                     where t.ToUpper().StartsWith("Б")
                     orderby t
                     select t).Count();
            // без мтеода Count
            var selectedTeams = from t in teams
                                where t.ToUpper().StartsWith("Б")
                                orderby t
                                select t;

            Console.WriteLine("Метод Count включен " + i); //3
            Console.WriteLine("Метод Count отдельно " + selectedTeams.Count()); //3

            teams[1] = "Ювентус";

            Console.WriteLine("\nМетод Count включен " + i); //3
            Console.WriteLine("Метод Count отдельно " + selectedTeams.Count()); //2
        }
    }
}