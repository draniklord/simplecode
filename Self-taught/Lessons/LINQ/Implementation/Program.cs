﻿using System;
using System.Linq;

namespace Implementation
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string[] teams = { "Бавария", "Боруссия", "Реал Мадрид", "Манчестер Сити", "ПСЖ", "Барселона" };

            var selectedTeams = 
                from t in teams 
                where t.ToUpper().StartsWith("Б") 
                orderby t 
                select t;

            // изменение массива после определения LINQ-запроса
            teams[1] = "Ювентус";

            // выполнение LINQ-запроса
            foreach (string s in selectedTeams)
                Console.WriteLine(s);
        }
    }
}