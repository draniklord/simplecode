﻿/*
    Требуется спарсить из массива строк - массив чисел.

    Реализуйте метод в одно LINQ выражение
    public static int[] ParseNumbers(IEnumerable<string> lines)
    {
        // linq выражение
    }
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Task_1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int[] result = ParseNumbers(new[] { "099", "-11", "2434", "800" });

            for (int i = 0; i < result.Length; i++)
            {
                Console.WriteLine(result[i]);
            }
        }

        public static int[] ParseNumbers(IEnumerable<string> lines)
        {
            return lines
                .Select(line => int.Parse(line))
                .ToArray();
        }
    }
}