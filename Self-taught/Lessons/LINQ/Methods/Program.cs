﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Methods
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<User> users = new List<User>();

            users.Add(new User { Id = 0, FirstName = "Ruslan", LastName = "Ivanov", Age = 18 });
            users.Add(new User { Id = 1, FirstName = "Ivan", LastName = "Petrov", Age = 21 });
            users.Add(new User { Id = 2, FirstName = "Denis", LastName = "Sidorov", Age = 19 });
            users.Add(new User { Id = 3, FirstName = "Timur", LastName = "Ivanov", Age = 22 });
            users.Add(new User { Id = 4, FirstName = "Alex", LastName = "Ivanov", Age = 34 });
            users.Add(new User { Id = 5, FirstName = "Oleg", LastName = "Ivanov", Age = 16 });

            //Метод Reverse
            Console.WriteLine("======Метод Reverse=====");

            users.Reverse();

            foreach (var user in users)
            {
                Console.WriteLine($"{user.FirstName} {user.LastName} {user.Age} лет");
            }

            //Метод All
            Console.WriteLine("\n=====Метод All=====");

            bool all = users.All(user => user.Age > 20);
            Console.WriteLine($"Все пользователи моложе 20 лет = {all}");

            //Метод Any
            Console.WriteLine("\n=====Метод Any=====");

            bool any = users.Any(user => user.Age > 20);
            Console.WriteLine($"Есть пользователь старше 20 лет = {any}");

            bool any2 = users.Any();
            Console.WriteLine($"В списке есть пользователи = {any2}");

            //Метод Contains
            Console.WriteLine("\n=====Метод Contains=====");

            bool contains = users.Select(user => user.Age).Contains(19);
            Console.WriteLine($"Есть пользователь(-и), которому 19 лет = {contains}");

            //Метод Count
            Console.WriteLine("\n=====Метод Count=====");

            int count = users.Count(user => user.Age < 25);
            Console.WriteLine($"Количество пользователей младше 25 лет = {count}");

            int count2 = users.Count();
            Console.WriteLine($"Количество пользователей = {count2}");

            //Метод Sum
            Console.WriteLine("\n=====Метод Sum=====");

            int sum = users.Select(user => user.Age).Sum();
            Console.WriteLine($"Сумма всех возрастов = {sum}");

            //Метод Average
            Console.WriteLine("\n=====Метод Sum=====");

            double average = users.Select(user => user.Age).Average();
            Console.WriteLine($"Средний возраст = {average}");

            //Методы Min и Max
            Console.WriteLine("\n=====Методы Min и Max=====");

            int min = users.Select(user => user.Age).Min();
            Console.WriteLine($"Минимальный возраст = {min}");

            int max = users.Select(user => user.Age).Max();
            Console.WriteLine($"Максимальный возраст = {max}");

            //Методы OrderBy и OrderByDescending
            Console.WriteLine("\n=====Методы OrderBy и OrderByDescending=====");

            Console.WriteLine("==Сортировка по возрастанию==");
            var orderBy = users.OrderBy(user => user.Age).ToList();
            foreach (var user in orderBy)
            {
                Console.WriteLine($"{user.FirstName} {user.LastName} {user.Age} лет");
            }

            Console.WriteLine("==Сортировка по убыванию==");
            var orderByDescending = users.OrderByDescending(user => user.Age).ToList();
            foreach (var user in orderByDescending)
            {
                Console.WriteLine($"{user.FirstName} {user.LastName} {user.Age} лет");
            }

            //Методы ThenBy и ThenByDescending
            Console.WriteLine("\n=====Методы ThenBy и ThenByDescending=====");

            var thenBy = users.OrderBy(user => user.Id).ThenBy(user => user.Age).ToList();
            foreach (var user in thenBy)
            {
                Console.WriteLine($"{user.FirstName} {user.LastName} {user.Age} лет");
            }
        }
    }
}