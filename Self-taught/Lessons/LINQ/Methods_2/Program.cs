﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Methods_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<User> users = new List<User>();

            users.Add(new User { Id = 0, FirstName = "Ruslan", LastName = "Ivanov", Age = 18 });
            users.Add(new User { Id = 1, FirstName = "Ivan", LastName = "Petrov", Age = 21 });
            users.Add(new User { Id = 2, FirstName = "Denis", LastName = "Sidorov", Age = 19 });
            users.Add(new User { Id = 3, FirstName = "Timur", LastName = "Ivanov", Age = 22 });
            users.Add(new User { Id = 4, FirstName = "Alex", LastName = "Ivanov", Age = 34 });
            users.Add(new User { Id = 5, FirstName = "Oleg", LastName = "Ivanov", Age = 16 });

            //Метод GroupBy
            Console.WriteLine("======Метод GroupBy=====");

            var groupBy = users.GroupBy(user => user.LastName);
            foreach (var g in groupBy)
            {
                Console.WriteLine(g.Key);

                foreach (var user in g)
                {
                    Console.WriteLine($"{user.FirstName} {user.Age}");
                }
                Console.WriteLine();
            }

            Dictionary<string, IGrouping<string, User>> groupDictionary = groupBy.ToDictionary(grouping => grouping.Key);

            Console.WriteLine("==========");

            var selectMany = groupBy.SelectMany(group => group);
            foreach (var user in selectMany)
            {
                Console.WriteLine($"{user.FirstName} {user.Age}");
            }

            //Метод Aggregate
            Console.WriteLine("\n======Метод Aggregate=====");

            int[] ints = new int[] { 1, 2, 3, 4 };

            var aggregate = ints.Aggregate((i, i1) => i + i1);
            var aggregate2 = ints.Aggregate((i, i1) => i - i1);

            Console.WriteLine("Сумма всех элементов массива: " + aggregate);
            Console.WriteLine("Разность всех элементов массива: " + aggregate2);

            //Метод Distinct
            Console.WriteLine("\n======Метод Distinct=====");

            int[] ints2 = new int[] {  4, 2, 3, 1, 1, 5, 5, 6 };

            var distinct = ints2.Distinct().ToList();

            foreach (var group in distinct)
            {
                Console.WriteLine(group);
            }

            //Метод Except
            Console.WriteLine("\n======Метод Except=====");

            int[] ints3 = new int[] { 7, 7, 7, 7, 7, 7, 7, 7 };

            var except = ints2.Except(ints3).ToList();

            foreach (var group in except)
            {
                Console.WriteLine(group);
            }

            //Метод Union
            Console.WriteLine("\n======Метод Union=====");

            var union = ints.Union(ints2).ToList();

            foreach (var item in union)
            {
                Console.WriteLine(item);
            }

            //Метод Intersect
            Console.WriteLine("\n======Метод Intersect=====");

            var intersect = ints.Intersect(ints2).ToList();

            foreach (var item in intersect)
            {
                Console.WriteLine(item);
            }

            //Метод Concat
            Console.WriteLine("\n======Метод Concat=====");

            var concat = ints.Concat(ints2).ToList();

            foreach (var item in concat)
            {
                Console.WriteLine(item);
            }

            //Методы First и FirstOrDefault
            Console.WriteLine("\n======Методы First и FirstOrDefault=====");

            var first = ints.First();
            Console.WriteLine(first);

            Console.WriteLine("===");

            var list = new List<int>();

            var firstOrDefault = list.FirstOrDefault();
            Console.WriteLine(firstOrDefault);

            //Методы Single и SingleOrDefault
            Console.WriteLine("\n======Методы Single и SingleOrDefault=====");

            var list2 = new List<int>();
            list2.Add(1);

            var single = list2.Single();
            Console.WriteLine(single);

            Console.WriteLine("===");

            var list3 = new List<int>();

            var singleOrDefault = list3.SingleOrDefault();
            Console.WriteLine(singleOrDefault);

            //Методы ElementAt и ElementAtOrDefault
            Console.WriteLine("\n======Методы ElementAt и ElementAtOrDefault=====");

            var elementAt = ints.ElementAt(0);
            Console.WriteLine(elementAt);

            var elementAt2 = users.ElementAt(0);
            Console.WriteLine(elementAt2.FirstName);

            Console.WriteLine("===");

            var elementAtOrDefault = list3.ElementAtOrDefault(0);
            Console.WriteLine(elementAtOrDefault);

            //Методы Last и LastOrDefault
            Console.WriteLine("\n======Методы Last и LastOrDefault=====");

            var last = users.Last(user => user.FirstName == "Ruslan");
            Console.WriteLine($"{last.LastName} {last.Age}");

            Console.WriteLine("===");

            var lastOrDefault = users.LastOrDefault(user => user.FirstName == "Brain");
            Console.WriteLine($"{lastOrDefault}");
        }
    }
}