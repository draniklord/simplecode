﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Delegates_3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<User> users = new List<User>();

            users.Add(new User { Id = 0, FirstName = "Ruslan", LastName = "Ivanov", Age = 18 });
            users.Add(new User { Id = 1, FirstName = "Ivan", LastName = "Petrov", Age = 21 });
            users.Add(new User { Id = 2, FirstName = "Denis", LastName = "Sidorov", Age = 19 });
            users.Add(new User { Id = 3, FirstName = "Timur", LastName = "Ivanov", Age = 22 });
            users.Add(new User { Id = 4, FirstName = "Alex", LastName = "Ivanov", Age = 34 });
            users.Add(new User { Id = 5, FirstName = "Oleg", LastName = "Ivanov", Age = 16 });

            //var result = new List<User>();
            //for (int i = 0; i < users.Count; i++)
            //{
            //    if (users[i].Age < 25)
            //    {
            //        result.Add(users[i]);
            //    }
            //}

            //for (int i = 0; i < result.Count; i++)
            //{
            //    Console.WriteLine($"{result[i].FirstName} {result[i].LastName} {result[i].Age}");
            //}

            // использование делегата
            var result1 = users
                .Where(user => user.Age < 25 && user.Age > 20);
            var result2 = result1
                .Select(user => user.Id);

            foreach (var user in result1)
            {
                Console.WriteLine($"{user.FirstName} {user.LastName} {user.Age}");
            }

            foreach (var userId in result2)
            {
                Console.WriteLine($"{userId}");
            }
        }
    }
}