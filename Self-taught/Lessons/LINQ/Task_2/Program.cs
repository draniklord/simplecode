﻿/*
 * Реализовать с использованием LINQ
 * 
 * ● Реализовать консольную программу:
    ○ Пользователь последовательно вводит 10 чисел
    ○ Программа формирует из них массив
    ○ Программа считает разницу между суммой четных и нечетных чисел, кроме отрицательных
    ○ Выводит результат в консоль
 * 
 * ● Доп задание:
    ○ Программа выводит исходный массив в обратном порядке
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Task_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();

            List<string> list = new List<string>();

            for (int i = 0; i < 10; i++)
            {
                list
                    .Add(random.Next(-100, 100)
                    .ToString());
            }

            var nums = list
                .Select(s => int.Parse(s))
                .ToArray();

            Console.WriteLine("Массив чисел");
            foreach (var num in nums)
            {
                Console.WriteLine(num);
            }

            var chetNums = nums
                .Where(i => i >= 0)
                .Where(i => i % 2 == 0)
                .Sum();

            var nechetNums = nums
                .Where(i => i >= 0)
                .Where(i => i % 2 != 0)
                .Sum();

            Console.WriteLine($"\nРазница между суммой четных и нечетных чисел, кроме отрицательных = {chetNums - nechetNums}\n");

            var reverse = nums.Reverse();

            Console.WriteLine("Обратный массив чисел");
            foreach (var num in reverse)
            {
                Console.WriteLine(num);
            }
        }
    }
}