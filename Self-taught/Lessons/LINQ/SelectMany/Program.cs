﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SelectMany
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string[] cars = { "Nissan", "Aston Martin", "Chevrolet", "Alfa Romeo", "Chrysler", "Dodge",
                "BMW", "Ferrari", "Audi", "Bentley", "Ford", "Lexus",
                "Mercedes", "Toyota", "Volvo", "Subaru", "Жигули" };

            //
            IEnumerable<char> chars = cars.SelectMany(p => p.ToArray());

            foreach (char c in chars)
                Console.Write(c + " ");

            Console.WriteLine("\n");

            //
            Employee[] employees = Employee.GetEmployeesArray();
            EmployeeOptionEntry[] empOptions = EmployeeOptionEntry.GetEmployeeOptionEntries();

            var employeeOptions = employees
              .SelectMany(e => empOptions
                                 .Where(eo => eo.id == e.id)
                                 .Select(eo => new
                                 {
                                     id = eo.id,
                                     optionsCount = eo.optionsCount
                                 }));

            foreach (var item in employeeOptions)
                Console.WriteLine(item);
        }
    }
}
