﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Query_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<User> users = new List<User>();

            users.Add(new User { Id = 0, FirstName = "Ruslan", LastName = "Ivanov", Age = 18 });
            users.Add(new User { Id = 1, FirstName = "Ivan", LastName = "Petrov", Age = 21 });
            users.Add(new User { Id = 2, FirstName = "Denis", LastName = "Sidorov", Age = 19 });
            users.Add(new User { Id = 3, FirstName = "Timur", LastName = "Ivanov", Age = 22 });
            users.Add(new User { Id = 4, FirstName = "Alex", LastName = "Ivanov", Age = 34 });
            users.Add(new User { Id = 5, FirstName = "Oleg", LastName = "Ivanov", Age = 16 });

            var resultQuery =
                from user in users
                where user.Age > 18
                select new
                {
                    Name = user.FirstName,
                    Age = user.Age,
                };

            foreach (var user in resultQuery)
            {

            }
        }
    }
}
