﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AsParallel_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string[] cars = { "Nissan", "Aston Martin", "Chevrolet", "Alfa Romeo", "Chrysler", "Dodge",
                "BMW", "Ferrari", "Audi", "Bentley", "Ford", "Lexus",
                "Mercedes", "Toyota", "Volvo", "Subaru", "Жигули"};

            // Последовательный запрос LINQ
            IEnumerable<string> auto = cars.Where(p => p.Contains("s"));

            foreach (string s in auto)
                Console.WriteLine("Результат последовательного запроса: " + s);

            // Запрос Parallel LINQ
            auto = cars.AsParallel()
                .Where(p => p.Contains("s"));

            foreach (string s in auto)
                Console.WriteLine("Результат параллельного запроса: " + s);
        }
    }
}