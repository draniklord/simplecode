﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Take
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string[] cars = { "Nissan", "Aston Martin", "Chevrolet", "Alfa Romeo", "Chrysler", "Dodge",
                "BMW", "Ferrari", "Audi", "Bentley", "Ford", "Lexus",
                "Mercedes", "Toyota", "Volvo", "Subaru", "Жигули" };

            IEnumerable<string> auto = cars.Take(5);

            foreach (string str in auto)
                Console.WriteLine(str);
        }
    }
}