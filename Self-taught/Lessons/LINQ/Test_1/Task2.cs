﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Test_1
{
    public static class Task2
    {
        public static void Method2(this List<int> list)
        {
            //Задание 2
            var nums2 = list
                .Where(i => i % 2 != 0)
                .OrderBy(order => order)
                .ToList()
                .Select(s => s.ToString());

            Console.WriteLine("Отсортированная последовательность");
            foreach (var item in nums2)
            {
                Console.WriteLine(item);
            }
        }
    }
}
