﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Test_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();

            List<int> list = new List<int>(10);

            for (int i = 0; i < 10; i++)
            {
                list.Add(random.Next(0, 50));
            }

            Console.WriteLine("Исходный массив");
            foreach (var item in list)
            {
                Console.WriteLine(item);
            }     

            // Задание 1
            list.Method1();
            
            // Задание 2
            list.Method2();

            // Задание 3
            List<Clients> clients = new List<Clients>();

            clients.Add(new Clients{ Id = 0, Year = 2021, Month = 2, Hours = 16});
            clients.Add(new Clients { Id = 1, Year = 2020, Month = 5, Hours = 4 });
            clients.Add(new Clients { Id = 2, Year = 2022, Month = 9, Hours = 18 });
            clients.Add(new Clients { Id = 3, Year = 2022, Month = 12, Hours = 22 });
            clients.Add(new Clients { Id = 4, Year = 2021, Month = 3, Hours = 4 });

            var result = clients
                .Where(last => last.Hours == clients.Select(s => s.Hours).Min())
                .Select(s => $"Продолжительность: {s.Hours} часа | Год: {s.Year} | Месяц: {s.Month}")
                .LastOrDefault();

            Console.WriteLine("\n" + result);
        }
    }
}