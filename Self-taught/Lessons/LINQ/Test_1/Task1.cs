﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Test_1
{
    public static class Task1
    {
        public static void Method1(this List<int> list)
        {
            // Задание 1
            var nums1 = list
                .Where(i => i % 2 != 0)
                .Distinct();

            Console.WriteLine("Нечетные числа последовательности");
            foreach (var num in nums1)
            {
                Console.WriteLine(num);
            }
        }
    }
}