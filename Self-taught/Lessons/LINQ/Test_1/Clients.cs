﻿namespace Test_1
{
    internal class Clients
    {
        public int Id { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int Hours { get; set; }
    }
}