﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Where
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // первый прототип Where
            string[] cars1 = { "Nissan", "Aston Martin", "Chevrolet", "Alfa Romeo", "Chrysler", "Dodge", "BMW",
       "Ferrari", "Audi", "Bentley", "Ford", "Lexus", "Mercedes", "Toyota", "Volvo", "Subaru", "Жигули"};

            IEnumerable<string> sequence1 = cars1.Where(p => p.StartsWith("F"));

            foreach (string s in sequence1)
                Console.WriteLine(s);

            Console.WriteLine();

            // второй прототип Where
            string[] cars2 = { "Nissan", "Aston Martin", "Chevrolet", "Alfa Romeo", "Chrysler", "Dodge", 
                "BMW", "Ferrari", "Audi", "Bentley", "Ford", "Lexus", 
                "Mercedes", "Toyota", "Volvo", "Subaru", "Жигули"};

            IEnumerable<string> sequence2 = cars2.Where((p, i) => (i & 1) == 1);

            foreach (string s in sequence2)
                Console.WriteLine(s);
        }
    }
}