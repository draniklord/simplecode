﻿using System;
using System.Threading;

namespace Synchronization_2
{
    public class MyTheard
    {
        private object threadLock = new object();

        public void ThreadNumbers()
        {
            // Используем маркер блокировки
            lock (threadLock)
            {
                // Информация о потоке
                Console.WriteLine("{0} поток использует метод ThreadNumbers", Thread.CurrentThread.Name);

                // Выводим числа
                Console.Write("Числа: ");
                for (int i = 0; i < 10; i++)
                {
                    Random rand = new Random();
                    Thread.Sleep(1000 * rand.Next(5));
                    Console.Write(i + ", ");
                }
                Console.WriteLine();
            }
        }
    }

    class Program
    {
        static void Main()
        {
            MyTheard mt = new MyTheard();

            // Создаем 10 потоков
            Thread[] threads = new Thread[10];

            for (int i = 0; i < 10; i++)
            {
                threads[i] = new Thread(new ThreadStart(mt.ThreadNumbers));
                threads[i].Name = string.Format("Работает поток: #{0}", i);
            }

            // Запускаем все потоки
            foreach (Thread t in threads)
                t.Start();
        }
    }
}