﻿using System;
using System.Threading;

namespace MyMonitor
{
    public class Sample
    {
        private object _lock = new object();
        public int counter;

        public void DoSmth()
        {
            try
            {
                Monitor.Enter(_lock);
                counter = counter + 1;
            }
            finally
            {
                Monitor.Exit(_lock);
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Sample sample = new Sample();

            for (int i = 0; i < 100; i++)
            {
                var i1 = i;
                var thread2 = new Thread(sample.DoSmth);
                thread2.Start();
            }

            //Thread.Sleep(1000);
            Console.WriteLine($"Counter: {sample.counter}");
        }
    }
}