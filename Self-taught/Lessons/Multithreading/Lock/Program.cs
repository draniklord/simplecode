﻿using System;
using System.Threading;

namespace Lock
{
    public class Sample
    {
        private object _lock = new object();
        public int counter;

        public void DoSmth()
        {
            lock (_lock)
            {
                counter = counter + 1;
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Sample sample = new Sample();

            for (int i = 0; i < 100; i++)
            {
                var i1 = i;
                var thread2 = new Thread(sample.DoSmth);
                thread2.Start();
            }

            //Thread.Sleep(1000);
            Console.WriteLine($"Counter: {sample.counter}");
        }
    }
}