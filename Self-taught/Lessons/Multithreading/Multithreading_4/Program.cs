﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;

namespace Multithreading_4
{
    public class Sample
    {
        private readonly object _lock = new object();
        private readonly ConcurrentQueue<Users> _users = new ConcurrentQueue<Users>();
        private Users _currentUsers;

        public void DoSmth()
        {
            lock (_lock)
            {
                //_currentUsers = _users.TryDequeue(result);
                _currentUsers.Age = 10;
                _currentUsers.FirstName = "Ivan";
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Sample sample = new Sample();

            for (int i = 0; i < 100; i++)
            {
                var i1 = i;
                var thread2 = new Thread(sample.DoSmth);
                thread2.Start();
            }

            //Thread.Sleep(1000);
            Console.WriteLine($"Counter: {sample}");
        }
    }
}