﻿using System;
using System.Threading;

namespace Multithreading_3
{
    public class Sample
    {
        public void DoSmth()
        {

        }
    }

    class Program
    {
        private static int counter;

        static void Main(string[] args)
        {
            Sample sample = new Sample();

            for (int i = 0; i < 100; i++)
            {
                var i1 = i;
                var thread2 = new Thread(() => Print1(i1));
                thread2.Start();
            }

            Console.WriteLine($"Counter: {counter}");
        }

        public static void Print1(int i)
        {
            counter++;
        }
    }
}