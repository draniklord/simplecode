﻿using System;
using System.Threading;

namespace Multithreading_2
{
    class Program
    {
        static void Main()
        {
            Console.Title = "Информация о главном потоке программы";

            Thread thread = Thread.CurrentThread;
            thread.Name = "MyThread";

            Console.WriteLine($"Имя домена приложения: {Thread.GetDomain().FriendlyName} " +
                $"\nID контекста: {Thread.CurrentContext.ContextID} " +
                $"\nИмя потока: {thread.Name} " +
                $"\nЗапущен ли поток?:  {thread.IsAlive}" +
                $"\nПриоритет потока: {thread.Priority}" +
                $"\nСостояние потока: {thread.ThreadState}");
        }
    }
}