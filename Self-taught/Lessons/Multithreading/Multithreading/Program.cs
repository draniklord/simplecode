﻿using System;
using System.Threading;

namespace Multithreading
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Print1();
            //Print2();

            //var threadStart = new ThreadStart(Print2);
            var thread1 = new Thread(Print2);
            thread1.Start();

            Console.WriteLine("\n");

            for (int i = 0; i < 100; i++)
            {
                var i1 = i;
                var thread2 = new Thread( () => Print1(i1));
                thread2.Start();
            }
        }

        public static void Print1()
        {
            Console.WriteLine("Start Print1");
            Thread.Sleep(1000);
            Console.WriteLine($"{nameof(Print1)} {Thread.CurrentThread.ManagedThreadId}");
        }
        public static void Print1(int i)
        {
            Console.WriteLine("Start Print1");
            Thread.Sleep(1000);
            Console.WriteLine($"{nameof(Print1)} {i} Thread: {Thread.CurrentThread.ManagedThreadId}");
        }


        public static void Print2()
        {
            Console.WriteLine("Start Print2");
            Console.WriteLine($"{nameof(Print2)} {Thread.CurrentThread.ManagedThreadId}");
        }

    }
}