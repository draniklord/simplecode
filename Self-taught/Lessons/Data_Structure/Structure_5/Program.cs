﻿using System;
using System.Collections.Generic;

namespace Structure_5
{
    class Program
    {
        static void Main()
        {
            Queue<int> qe = new Queue<int>();
            Random ran = new Random();

            for (int i = 0; i < 10; i++)
                qe.Enqueue(ran.Next(1, 10));

            Console.Write("Очередь: ");
            foreach (int i in qe)
                Console.Write("\t" + i);
        }
    }
}