﻿using System;
using System.Collections.Generic;

namespace Structure_1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            System.Collections.Generic.List<string> linkedList = new System.Collections.Generic.List<string>();

            // добавление элементов
            linkedList.Add("Tom");
            linkedList.Add("Alice");
            linkedList.Add("Bob");
            linkedList.Add("Sam");

            // выводим элементы
            foreach (var item in linkedList)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();

            // удаляем элемент
            linkedList.Remove("Alice");

            foreach (var item in linkedList)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();

            // проверяем наличие элемента
            bool isPresent = linkedList.Contains("Sam");

            Console.WriteLine(isPresent == true ? "Sam присутствует" : "Sam отсутствует");
            Console.WriteLine();

            // добавляем элемент           
            linkedList.Add("Bill");

            foreach (var item in linkedList)
            {
                Console.WriteLine(item);
            }
        }
    }
}