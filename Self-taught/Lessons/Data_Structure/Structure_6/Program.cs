﻿using System;
using System.Collections.Generic;

namespace Structure_6
{
    class Program
    {
        static void Main()
        {
            var people = new Stack<string>();
            people.Push("Tom");
            people.Push("Sam");
            people.Push("Bob");

            Console.WriteLine("Исходный стек: ");
            foreach (string s in people)
                Console.Write("\t" + s);

            Console.WriteLine("\n");

            Console.WriteLine("Людей в очереди: " + people.Count + "\n");

            people.Pop();

            while (people.Count > 0)
            {
                Console.WriteLine("Pop -> {0}", people.Pop());
            }

            if (people.Count == 0)
                Console.WriteLine("\nСтек пуст!");
        }
    }
}