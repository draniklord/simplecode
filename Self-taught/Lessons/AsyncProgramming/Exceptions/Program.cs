﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Exceptions
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine($"Start | Thread: {Thread.CurrentThread.ManagedThreadId}");
            try
            {
               await M1();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }

            Console.WriteLine($"Stop | Thread: {Thread.CurrentThread.ManagedThreadId}");
        }

        public static async Task M1()
        {
            Console.WriteLine($"Start M1 | Thread: {Thread.CurrentThread.ManagedThreadId}");

            await Task.Run(Actions);

            throw new Exception();

            Console.WriteLine($"Finish M1 | Thread: {Thread.CurrentThread.ManagedThreadId}\n");
        }

        public static async Task M2()
        {
            Console.WriteLine($"Start M2 | Thread: {Thread.CurrentThread.ManagedThreadId}");

            await Task.Run(Actions);

            Console.WriteLine($"Finish M2 | Thread: {Thread.CurrentThread.ManagedThreadId}\n");
        }


        private static async Task Actions()
        {
            await Task.Delay(1000);

            Console.WriteLine($"Task | Thread: {Thread.CurrentThread.ManagedThreadId}");
        }
    }
}