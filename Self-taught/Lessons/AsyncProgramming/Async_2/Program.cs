﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Async_2
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine($"Start | Thread: {Thread.CurrentThread.ManagedThreadId}");

            await M1();
            //Console.WriteLine();
            await M2();

            Console.WriteLine();

            await Task.WhenAll(M1(), M2());

            Console.WriteLine();

            await Task.WhenAny(M1(), M2());

            Console.WriteLine($"Stop | Thread: {Thread.CurrentThread.ManagedThreadId}");
        }

        public static async Task M1()
        {
            Console.WriteLine($"Start M1 | Thread: {Thread.CurrentThread.ManagedThreadId}");

            await Task.Run(Actions);

            Console.WriteLine($"Finish M1 | Thread: {Thread.CurrentThread.ManagedThreadId}\n");
        }

        public static async Task M2()
        {
            Console.WriteLine($"Start M2 | Thread: {Thread.CurrentThread.ManagedThreadId}");

            await Task.Run(Actions);

            Console.WriteLine($"Finish M2 | Thread: {Thread.CurrentThread.ManagedThreadId}\n");
        }


        private static async Task Actions()
        {
            await Task.Delay(1000);

            Console.WriteLine($"Task | Thread: {Thread.CurrentThread.ManagedThreadId}");
        }
    }
}