﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Async_3
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Готовим завтрак\n");

            await PourCoffee();

            await Task.WhenAll(FryEggs(), FryBacon(), ToastBread());

            //await FryEggs();
            //await FryBacon();
            //await ToastBread();

            await JamOnBread();

            PourJoice();

            Console.WriteLine("Завтрак готов!");
        }

        public static async Task PourCoffee()
        {
            Console.WriteLine("Начали готовить кофе");
            await Task.Delay(2000);
            Console.WriteLine("Закончили готовить кофе\n");
        }

        public static async Task FryEggs()
        {
            Console.WriteLine("Начали жарить яйца");
            await Task.Delay(2000);
            Console.WriteLine("Закончили жарить яйца\n");
        }

        public static async Task FryBacon()
        {
            Console.WriteLine("Начали жарить бекон");
            await Task.Delay(2000);
            Console.WriteLine("Закончили жарить бекон\n");
        }

        public static async Task ToastBread()
        {
            Console.WriteLine("Начали жарить тосты");
            await Task.Delay(2000);
            Console.WriteLine("Закончили жарить тосты\n");
        }

        public static async Task JamOnBread()
        {
            Console.WriteLine("Начали мазать джем");
            await Task.Delay(2000);
            Console.WriteLine("Закончили мазать джем\n");
        }

        public static void PourJoice()
        {
            Console.WriteLine("Налили сок");;
        }
    }
}
