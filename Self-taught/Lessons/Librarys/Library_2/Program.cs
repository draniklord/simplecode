﻿using System;
using UtilityLibraries;

class Program
{
    static void Main(string[] args)
    {
        int row = 0;

        do
        {
            if (row == 0 || row >= 25)
                ResetConsole();

            string input = Console.ReadLine();
            if (string.IsNullOrEmpty(input)) break;
            Console.WriteLine($"Введено: {input} {"Начинается с заглавной буквы? ",30}: " +
                              $"{(input.StartsWithUpper() ? "Да" : "Нет")}{Environment.NewLine}");
            row += 3;
        } while (true);
        return;

        // Объявить локальный метод ResetConsole
        void ResetConsole()
        {
            if (row > 0)
            {
                Console.WriteLine("Нажмите любую клавишу чтобы продолжить...");
                Console.ReadKey();
            }
            Console.Clear();
            Console.WriteLine($"{Environment.NewLine}" +
                $"Нажмите <Enter> только для выхода; в противном случае введите строку и нажмите <Enter>:" +
                $"{Environment.NewLine}");
            row = 3;
        }
    }
}