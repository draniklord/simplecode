﻿using System;

namespace LibraryExtensions
{
    public static class IntArrayExtensions
    {
        public static void Reset(this int[] intArray)
        {
            for (int i = 0; i < intArray.Length; i++)
            {
                intArray[i] = 0;
            }
        }

        public static void Reset(this int[] intArray, int value)
        {
            for (int i = 0; i < intArray.Length; i++)
            {
                intArray[i] = value;
            }
        }

        public static void Randomizer(this int[] intArray)
        {
            Random random = new Random();

            for (int i = 0; i < intArray.Length; i++)
            {
                intArray[i] = random.Next(1, 100);
            }
        }
    }
}