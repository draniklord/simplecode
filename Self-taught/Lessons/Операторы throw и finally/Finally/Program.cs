﻿using System;

namespace Finally
{
    internal class Program
    {
        static void Del(int x, int y)
        {
            try
            {
                int result = x / y;
            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("Деление на ноль!");
            }
            // Данный блок будет всегда выполняться
            finally
            {
                Console.WriteLine("Конец программы");
            }
        }

        static void Main()
        {
            Del(5, 0);
        }
    }
}