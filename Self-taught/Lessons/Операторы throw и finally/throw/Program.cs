﻿using System;

namespace Throw
{
    class Program
    {
        static void Main()
        {
            Console.Write("Введите число: ");
            int i = int.Parse(Console.ReadLine());
            byte j;
            try
            {
                if (i > 255)
                    // Генерируем исключение
                    throw new OverflowException();
                else
                    j = (byte)i;
            }
            catch (OverflowException)
            {
                Console.WriteLine("Возникло переполнение");
            }
        }
    }
}