using System;
using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer.Models
{
    public class Ticket
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        
        public int Price { get; set; }
        
        public DateTime Date { get; set; }
        
        public int UserId { get; set; }

        public virtual User User { get; set; }
    }
}