using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.Models
{
    [Table("Custmers")]
    public class User
    {
        [Required]
        public string Login { get; set; }
        
        [Required]
        public string Password { get; set; }

        [Key]
        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }
        
        [Required]
        public string LastName { get; set; }

        public Role Role { get; set; }

        public string PhoneNumber { get; set; }

        public int Age { get; set; }

        public virtual ICollection<Ticket> Tickets { get; set; }

        public string FullName
        {
            get
            {
                return $"{FirstName} {LastName}";
            }
        }
    }
}