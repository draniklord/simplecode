using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.Models
{
    public class CustomTicket : Ticket
    {
        public string Description { get; set; }
    }
}