using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations
{
    public class TicketConfiguration : IEntityTypeConfiguration<Ticket>
    {
        public void Configure(EntityTypeBuilder<Ticket> builder)
        {
            builder.ToTable("Tickets");
            builder.HasKey(ticket => ticket.Id);
            builder.HasIndex(ticket => ticket.Name);
            builder
                .HasOne(ticket => ticket.User)
                .WithMany(user => user.Tickets)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}