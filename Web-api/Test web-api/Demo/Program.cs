﻿// See https://aka.ms/new-console-template for more information

using DataAccessLayer;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;

var optionsBuilder = new DbContextOptionsBuilder<DataContext>().UseNpgsql("Host=localhost;Port=5432;Database=test1;Username=postgres;Password=postgres");
var context = new DataContext(optionsBuilder.Options);
context.Database.Migrate();

// context.Users.Add(new User()
// {
//     Role = Role.Admin,
//     FirstName = "Ivan",
//     LastName = "Ivanov"
// });

var firstUser = context.Users.AsNoTracking().FirstOrDefault();
if (firstUser != null) 
    firstUser.FirstName = "Petr";

context.SaveChanges();
context.Dispose();

Console.WriteLine("Success!");
Console.ReadKey();