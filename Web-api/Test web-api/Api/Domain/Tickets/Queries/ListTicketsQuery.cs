using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Api.Models;
using AutoMapper;
using DataAccessLayer;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Api.Domain.Tickets.Queries
{
    public class ListTicketsQuery : IRequest<IEnumerable<TicketDto>>
    {
        public int Skip { get; set; }
        public int Take { get; set; }
    }
    
    public class ListTicketsQueryValidator : AbstractValidator<ListTicketsQuery>
    {
        public ListTicketsQueryValidator()
        {
            RuleFor(query => query.Skip).GreaterThanOrEqualTo(0).LessThanOrEqualTo(100);
            RuleFor(query => query.Take).GreaterThanOrEqualTo(1).LessThanOrEqualTo(100);
        }
    }

    public class ListTicketsQueryHandler : IRequestHandler<ListTicketsQuery, IEnumerable<TicketDto>>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public ListTicketsQueryHandler(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<IEnumerable<TicketDto>> Handle(ListTicketsQuery request, CancellationToken cancellationToken)
        {
            var ticketsData = await _dataContext.Tickets
                .Skip(request.Skip)
                .Take(request.Take)
                .Include(ticket => ticket.User)
                .AsNoTracking()
                .ToListAsync(cancellationToken);

            return _mapper.Map<IEnumerable<TicketDto>>(ticketsData);
        }
    }
}