namespace Api.Domain.Tickets
{
    public class TicketDto
    {
        public int Id { get; set; }
        
        public string Name { get; set; }
    }
}