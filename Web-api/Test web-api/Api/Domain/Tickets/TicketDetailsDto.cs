using System;
using Api.Models;

namespace Api.Domain.Tickets
{
    /// <summary>
    /// Билет на фильм
    /// </summary>
    public class TicketDetailsDto
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; set; }
        
        /// <summary>
        /// Наименование фильма
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Цена билета
        /// </summary>
        public int Price { get; set; }
        
        /// <summary>
        /// ДАта и время сеанса
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Информация о покупателе билета
        /// </summary>
        public virtual UserDto User { get; set; }
    }
}