using AutoMapper;
using DataAccessLayer.Models;

namespace Api.Models.MapProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserDto, User>()
                .ForMember(user => user.Role, expression => expression
                    .MapFrom(dto => (Role) dto.Role))
                .ForMember(user => user.FirstName, expression => expression
                    .MapFrom(dto => dto.Name));
            
            CreateMap<User, UserDto>()
                .ForMember(dto => dto.Role, expression => expression
                    .MapFrom(user => (int) user.Role))
                .ForMember(dto => dto.Name, expression => expression
                    .MapFrom(user => user.FirstName));
        }
    }
}