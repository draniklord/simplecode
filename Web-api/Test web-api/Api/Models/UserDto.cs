using FluentValidation;

namespace Api.Models
{
    public class UserDto
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public int Role { get; set; }
    }
    
    public class UserValidator : AbstractValidator<UserDto>
    {
        public UserValidator()
        {
            RuleFor(user => user.Name).NotNull().NotEmpty().MaximumLength(20);
            RuleFor(user => user.LastName).NotNull().NotEmpty().MaximumLength(20);
            RuleFor(user => user.Role).LessThan(3).GreaterThan(-2);
        }
    }
}