using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Api.Models
{
    public class NameValidationAttribute : ValidationAttribute
    {
        public readonly List<string> names;
        
        public NameValidationAttribute(params string[] names)
        {
            this.names = names.ToList();
        }

        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            if (value is string str)
            {
                if(names.Contains(str))
                    return ValidationResult.Success;
            }

            return new ValidationResult("Недопустимое имя!");
        }
    }
}