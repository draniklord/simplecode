using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Api.Models
{
    public class Ticket : IValidatableObject
    {
        [Range(0,1000)]
        public int Id { get; set; }
        
        [Required(ErrorMessage = "Name is required.")]
        [StringLength(20)]
        [NameValidation("spider-man", "interstellar")]
        public string Name { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Name != "spider-man")
            {
                return new List<ValidationResult>()
                {
                    new ValidationResult("Имя должно быть spider-man")
                };
            }

            return new List<ValidationResult>()
            {
                ValidationResult.Success
            };
        }
    }
}