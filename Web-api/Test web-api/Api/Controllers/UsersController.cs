using System.Threading.Tasks;
using Api.Models;
using Api.Services;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public ActionResult GetAll()
        {
            return Ok(_userService.GetAll());
        }
        
        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            return Ok(_userService.Get(id));
        }
        
        [HttpPost]
        public async Task Create(UserDto userDto)
        {
            await _userService.Create(userDto);
        }
        
        [HttpDelete("{id}")]
        public ActionResult Update(int id)
        {
            var data = _userService.Get(id);
            if (data == null)
                return NotFound();
            
            _userService.Delete(id);
            return Ok();
        }
    }
}