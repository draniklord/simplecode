using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Domain.Tickets;
using Api.Domain.Tickets.Queries;
using Api.Models;
using DataAccessLayer;
using DataAccessLayer.Models;
using MediatR;
using Domain;
using Domain.Tickets;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Shared.Models;
using CreateTicketCommand = Api.Domain.Tickets.Commands.CreateTicketCommand;
using TicketDto = Api.Domain.Tickets.TicketDto;

namespace Api.Controllers
{
    public class ApiError : ActionResult
    {
        /// <summary>
        /// Код ошибки
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// Содержание ошибки
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Получение результата
        /// </summary>
        /// <returns> код результата </returns>
        public int GetResult()
        {
            return 0;
        }
        
        /// <summary>
        /// Получение результата
        /// </summary>
        /// <param name="args"> Аргументы к методу</param>
        /// <returns> код результата </returns>
        public int GetResult(string args)
        {
            return 0;
        }
    }

    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class TicketsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public TicketsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IEnumerable<TicketDto>> GetAll([FromQuery]ListTicketsQuery ticketsQuery)
        {
            var result = await _mediator.Send(ticketsQuery);
            return result;
        }
        
        /// <summary>
        /// Получение ticket по номеру
        /// </summary>
        /// <param name="id"></param>
        /// <response code="200"> Билет найден </response>
        /// <response code="500"> Билет не найден </response>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(TicketDetailsDto), 200)]
        [ProducesResponseType(typeof(ApiError), 500)]
        public async Task<ActionResult> Get([FromRoute]int id)
        {
            TicketDetailsDto result;
            var query = new Details(id);
            try
            { 
                result = await _mediator.Send(query);
            }
            catch (Exception e)
            {
                var error = new ApiError();
                error.Message = "Возникла ошибка! Ticket не найден";
                error.GetResult("");
                return error;
            }
            
            return Ok(result);
        }
        
        // [HttpDelete("{id}")]
        // public void Delete(int id)
        // {
        //     throw new NotImplementedException();
        // }
        
        [HttpPost]
        public async Task<ActionResult> Create(string name, int userId)
        {
            if (ModelState.IsValid)
            {
                var createTicketCommand = new CreateTicketCommand(name, userId);
                await _mediator.Send(createTicketCommand);
                return Ok();
            }
        
            return BadRequest(ModelState);
        }
        
        // [HttpPut("{id}")]
        // public void Edit(int id, [FromQuery]string ticketName)
        // {
        //     throw new NotImplementedException();
        // }
    }
}