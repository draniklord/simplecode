using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Api.Domain.Accounts.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Serilog;

namespace Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class AccountsController : ControllerBase
    {
        private readonly ILogger<AccountsController> _logger;
        private readonly IMediator _mediator;

        public AccountsController(IMediator mediator, ILogger<AccountsController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [AllowAnonymous]
        [HttpGet("/login")]
        public async Task<ActionResult<string>> Login(string login, string password)
        {
            _logger.LogInformation($"{nameof(Login)} GET request!");
            try
            {
                var jwt = await _mediator.Send(new LoginRequest(login, password));
                return Ok(jwt);
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }
        
        [AllowAnonymous]
        // [Authorize(Roles = "User,Admin")]
        [HttpGet("/me")]
        public ActionResult Me()
        {
            _logger.LogInformation($"{nameof(Me)} GET request!");
            return Ok(HttpContext.User.Claims?.FirstOrDefault(x => x.Type == ClaimsIdentity.DefaultNameClaimType)?.Value);
        }
        
        [Authorize(Policy = Policies.RequireAdultPolicy)]
        [HttpGet("/age")]
        [ResponseHeader("My-header", "9958")]
        public ActionResult GetAge()
        {
            var claims = HttpContext.User.Claims;
            return Ok(HttpContext.User.Claims?.FirstOrDefault(x => x.Type == "age")?.Value);
        }
    }
}