namespace Api
{
    public class ApiResult<T> : ApiResult
    {
        public T Data { get; set; }
    }
    
    public class ApiResult
    {
        public bool Success { get; set; }
        public int StatusCode { get; set; }
        public string Errors { get; set; }
    }
}