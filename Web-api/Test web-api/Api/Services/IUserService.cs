using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Models;

namespace Api.Services
{
    public interface IService<T>
    {
        Task Create(T userDto);
        void Delete(int id);
        T Get(int id);
        IEnumerable<T> GetAll();
    }
    
    public interface IUserService : IService<UserDto>
    {
    }
}