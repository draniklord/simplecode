using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Models;
using AutoMapper;
using DataAccessLayer;
using DataAccessLayer.Models;

namespace Api.Services
{
    public class UserService : IUserService
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public UserService(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task Create(UserDto userDto)
        {
            var user = _mapper.Map<User>(userDto);
            await _dataContext.Users.AddAsync(user);
            await _dataContext.SaveChangesAsync();
        }

        public void Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public UserDto Get(int id)
        {
            var user = _dataContext.Users.First(user1 => user1.Id == id);
            return _mapper.Map<UserDto>(user);
        }

        public IEnumerable<UserDto> GetAll()
        {
            throw new System.NotImplementedException();
        }
    }
}