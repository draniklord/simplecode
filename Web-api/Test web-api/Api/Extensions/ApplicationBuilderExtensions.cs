using Api.Middlewares;
using Microsoft.AspNetCore.Builder;

namespace Api.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseExceptionsHandler(this IApplicationBuilder applicationBuilder)
        {
            applicationBuilder.UseMiddleware<ExceptionMiddleware>();
            return applicationBuilder;
        }
        
        public static IApplicationBuilder UseTokenAuth(this IApplicationBuilder applicationBuilder, string token)
        {
            applicationBuilder.UseMiddleware<TokenMiddleware>(token);
            return applicationBuilder;
        }
    }
}