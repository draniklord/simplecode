using System;
using System.IO;
using System.Text;
using Api.Extensions;
using Api.Models;
using Api.Models.MapProfiles;
using Api.Services;
using DataAccessLayer;
using Domain.Tickets;
using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Serilog;
using CreateTicketCommand = Api.Domain.Tickets.Commands.CreateTicketCommand;
using TicketProfile = Api.Domain.Tickets.TicketProfile;

namespace Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var appOptions = Configuration.Get<AppOptions>();

            services.AddDbContext<DataContext>(builder => builder
                .UseLazyLoadingProxies()
                .UseNpgsql(appOptions.DbConnection));

            services.Configure<AppOptions>(Configuration);
            services.AddScoped<IHelloService, HelloService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IJwtGenerator, JwtGenerator>();
            services.AddScoped<IAuthorizationHandler, AdultAuthRequirementHandler>();
            services.AddMediatR(typeof(CreateTicketCommand));
            
            services.AddControllers()
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<UserValidator>());

            services.AddAutoMapper(typeof(UserProfile), typeof(TicketProfile));
            
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "WebApplication3", Version = "v1" });
                c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "Api.xml"));
                
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In           = ParameterLocation.Header,
                    Description  = "Please insert token",
                    Name         = "Authorization",
                    Type         = SecuritySchemeType.ApiKey,
                    BearerFormat = "JWT"
                });
                
                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference {Type = ReferenceType.SecurityScheme, Id = "Bearer"}
                        },
                        new string[] { }
                    }
                });
            });

            services
                .AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme  = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme     = JwtBearerDefaults.AuthenticationScheme;
                    x.RequireAuthenticatedSignIn = false;
                })
                .AddJwtBearer(x =>
                {
                    x.RequireHttpsMetadata = false;
                    x.SaveToken            = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes("my-secret-key-key-key-key-key-key-key-key")),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                });
            
            services.AddAuthorization(options => 
                options.AddPolicy("RequireAdminRole", builder => builder.RequireRole("Admin")));
            
            services.AddAuthorization(options => 
                options.AddPolicy(Policies.RequireAdultPolicy, 
                    builder => builder.AddRequirements(new AdultAuthRequirement(18))));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IOptions<AppOptions> options)
        {
            if (options.Value.SwaggerEnabled)
            {
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "WebApplication3 v1"));
                app.UseReDoc(c => c.RoutePrefix = "docs");
            }
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // app.UseExceptionsHandler();
            
            app.UseHttpsRedirection();
            
            app.UseRouting();

            app.UseAuthentication();
            
            app.UseAuthorization();

            // app.UseTokenAuth("555");
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/hello", context => context.Response.WriteAsync("Hello!"));
            });
        }
    }
}