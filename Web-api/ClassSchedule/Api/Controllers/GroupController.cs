﻿using Api.Domain.Groups;
using Api.Domain.Groups.Command;
using Api.Domain.Groups.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class GroupController : ControllerBase
    {
        private IMediator _mediator;

        public GroupController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Получение данных о группах
        /// </summary>
        /// <param name="getGroupQuery"></param>
        /// <responce code="200">Группы найлены</responce>
        /// <responce code="204">Группы не найдены</responce>
        /// <returns></returns>
        [ProducesResponseType(typeof(GroupDto), 200)]
        [ProducesResponseType(typeof(ApiError), 204)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GroupDto>>> GetAllGroup([FromQuery] GetGroupQuery getGroupQuery)
        {
            try
            {
                var result = await _mediator.Send(getGroupQuery);
                return Ok(result);
            }
            catch (Exception)
            {
                if (Response.StatusCode == 204)
                {
                    var error = new ApiError
                    {
                        Code = 204,
                        Message = "Группа с таким ID отсутствует!"
                    };
                    return BadRequest(error);
                }
                return Ok();
            }
        }

        /// <summary>
        /// Поиск группы по ID
        /// </summary>
        /// <param name="id"></param>
        /// <responce code="200">Группы найлены</responce>
        /// <responce code="204">Группы не найдены</responce>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Produces("application/json")]
        [CustomExceptionFilter]
        [ProducesResponseType(typeof(GroupDto), 200)]
        //[ProducesResponseType(typeof(ApiError), 204)]
        //[ProducesResponseType(typeof(ApiError), 500)]
        public async Task<IActionResult> GetIdGroup(int id)
        {
            GroupDto result;
            var query = new GetIdGroup(id);
            result = await _mediator.Send(query);

            var error = new ApiError();
           
            return Ok(result);

            //try
            //{
            //    result = await _mediator.Send(query);
            //    return Ok(result);
            //}
            //catch (Exception)
            //{
            //    if (Response.StatusCode == 204)
            //    {
            //        error.Code = 204;
            //        error.Message = "Группа с таким ID отсутствует!";
            //        //return error;
            //    }
            //    return error;
            //    //throw;
            //}
        }

        /// <summary>
        /// Создать новую группу
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [ProducesResponseType(typeof(GroupDto), 200)]
        [ProducesResponseType(typeof(ApiError), 400)]
        [HttpPost]
        public async Task<ActionResult> CreateGroup(int id, string name)
        {
            var error = new ApiError();

            if (ModelState.IsValid)
            {
                var createGroup = new GroupCreate(id, name);
                await _mediator.Send(createGroup);
                return Ok();
            }

            if (Response.StatusCode == 400)
            {
                new ApiError
                {
                    Code = 400,
                    Message = "Группа с таким ID уже существует!"
                };
                //return BadRequest(error);
            }

            return BadRequest(error);
        }
    }
}
