﻿using Api.Domain.Groups;
using AutoMapper;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using System.Linq;

namespace Api.Domain.GraduatingDepartments.Queries
{
    public class GetAll : IRequest<IEnumerable<GraduatingDepartmentDto>>
    {
        public class GetAllQueryHandler : IRequestHandler<GetAll, IEnumerable<GraduatingDepartmentDto>>
        {
            private readonly DataContext _dataContext;
            private readonly IMapper _mapper;

            public GetAllQueryHandler(DataContext dataContext, IMapper mapper)
            {
                _dataContext = dataContext;
                _mapper = mapper;
            }
            public async Task<IEnumerable<GraduatingDepartmentDto>> Handle(GetAll query, CancellationToken cancellationToken)
            {
                var includableQueryable = _dataContext.GraduatingDepartments
                                    .Include(direction => direction.DirectionsOfStudy)
                                    .ThenInclude(group => group.Groups)
                                    .ThenInclude(student => student.Student);

                var departamenttList = await includableQueryable
                    .Include(direction => direction.DirectionsOfStudy)
                    .ThenInclude(discipline => discipline.Disciplines)
                    .ThenInclude(teacher => teacher.Teachers)
                    .AsNoTracking()
                    .ToListAsync(cancellationToken);

                if (departamenttList == null)
                {
                    return null;
                }
                //return (IEnumerable<GraduatingDepartmentDto>)departamenttList.AsReadOnly();
                return _mapper.Map<IEnumerable<GraduatingDepartmentDto>>(departamenttList);
            }
        }
    }
}
