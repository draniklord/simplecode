﻿using AutoMapper;
using DataAccessLayer.Models;

namespace Api.Domain.Teachers
{
    public class TeacherProfile : Profile
    {
        public TeacherProfile()
        {
            CreateMap<TeacherDto, Teacher>();
            CreateMap<Teacher, TeacherDto>();
        }
    }
}
