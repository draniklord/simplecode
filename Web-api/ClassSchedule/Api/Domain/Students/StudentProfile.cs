﻿using AutoMapper;
using DataAccessLayer.Models;

namespace Api.Domain.Students
{
    public class StudentProfile : Profile
    {
        public StudentProfile()
        {
            CreateMap<StudentDto, Student>();
            CreateMap<Student, StudentDto>();
        }
    }
}
