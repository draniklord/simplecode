﻿using Api.Domain.Groups;

namespace Api.Domain.Students
{
    /// <summary>
    /// Студент
    /// </summary>
    public class StudentDto
    {
        /// <summary>
        /// Индивидуальный номер студента
        /// </summary>
        public int StudentId { get; set; }

        /// <summary>
        /// Имя студента
        /// </summary>
        public string? FirstName { get; set; }

        /// <summary>
        /// Фамилия студента
        /// </summary>
        public string? LastName { get; set; }

        /// <summary>
        /// Группа, в которой учится студент
        /// </summary>
        public GroupDto? Group { get; set; }

        public Role Role { get; set; }
    }
}
