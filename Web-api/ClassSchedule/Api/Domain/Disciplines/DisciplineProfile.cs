﻿using AutoMapper;
using DataAccessLayer.Models;

namespace Api.Domain.Disciplines
{
    public class DisciplineProfile : Profile
    {
        public DisciplineProfile()
        {
            CreateMap<DisciplineDto, Discipline>();
            CreateMap<Discipline, DisciplineDto>();
        }
    }
}
