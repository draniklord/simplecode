﻿using DataAccessLayer;
using DataAccessLayer.Models;
using MediatR;

namespace Api.Domain.Disciplines.Command
{
    public class CreateDisciplines : IRequest
    {
        /// <summary>
        /// Номер дисциплины
        /// </summary>
        public int DisciplineId { get; set; }

        /// <summary>
        /// Название дисциплины
        /// </summary>
        public string? DisciplineName { get; set; }

        /// <summary>
        /// Номер группы по данной дисциплине
        /// </summary>
        public int GroupsId { get; set; }

        public CreateDisciplines(int disciplineId, string? disciplineName, int groupsId)
        {
            DisciplineId = disciplineId;
            DisciplineName = disciplineName;
            GroupsId = groupsId;
        }     
    }

    public class GroupCreateHandler : IRequestHandler<CreateDisciplines>
    {
        private DataContext _dataContext;

        public GroupCreateHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<Unit> Handle(CreateDisciplines request, CancellationToken cancellationToken)
        {
            await _dataContext.Disciplines.AddAsync(new Discipline()
            {
                DisciplineId = request.DisciplineId,
                DisciplineName = request.DisciplineName,
                GroupId = request.GroupsId

            }, cancellationToken);

            await _dataContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
