﻿using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Api.Domain.Disciplines.Command
{
    public class DeleteDiscipline : IRequest<int>
    {
        public int Id { get; set; }

        public class DeleteDisciplineHandler : IRequestHandler<DeleteDiscipline, int>
        {
            private readonly DataContext _dataContext;

            public DeleteDisciplineHandler(DataContext dataContext)
            {
                _dataContext = dataContext;
            }

            public async Task<int> Handle(DeleteDiscipline command, CancellationToken cancellationToken)
            {
                var discipline = await _dataContext.Disciplines
                    .Where(a => a.DisciplineId == command.Id)
                    .FirstOrDefaultAsync(cancellationToken);

                if (discipline == null) 
                    return default;

                _dataContext.Disciplines.Remove(discipline);

                _dataContext.SaveChanges();

                return (int)discipline.DisciplineId;
            }
        }
    }
}
