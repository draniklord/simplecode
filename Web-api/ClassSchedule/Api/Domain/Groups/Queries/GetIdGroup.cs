﻿using AutoMapper;
using DataAccessLayer;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Api.Domain.Groups.Queries
{
    public class GetIdGroup : IRequest<GroupDto>
    {
        public int GroupId { get; }

        public GetIdGroup(int groupId)
        {
            GroupId = groupId;
        }
    }

    public class DetailsValidation : AbstractValidator<GetIdGroup>
    {
        public DetailsValidation()
        {
            RuleFor(query => query.GroupId).GreaterThanOrEqualTo(1);
        }
    }

    public class DetailsHandler : IRequestHandler<GetIdGroup, GroupDto>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public DetailsHandler(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<GroupDto> Handle(GetIdGroup request, CancellationToken cancellationToken)
        {
            var group = await _dataContext.Groups
                .Include(student => student.Student)
                .Include(discipline => discipline.Discipline)
                .AsNoTracking()
                .FirstOrDefaultAsync(group1 => group1.GroupId == request.GroupId, cancellationToken: cancellationToken);

            return _mapper.Map<GroupDto>(group);
        }
    }
}
