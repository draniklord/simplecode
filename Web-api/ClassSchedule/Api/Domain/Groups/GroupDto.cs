﻿using Api.Domain.Disciplines;
using Api.Domain.Students;

namespace Api.Domain.Groups
{
    /// <summary>
    /// Группа
    /// </summary>
    public class GroupDto
    {
        /// <summary>
        /// Номер группы
        /// </summary>
        public int GroupId { get; set; }

        /// <summary>
        /// Название группы
        /// </summary>
        public string? GroupName { get; set; }

        /// <summary>
        /// Студенты в данной группе
        /// </summary>
        public List<StudentDto>? Students { get; set; }

        /// <summary>
        /// Дисциплины у данной группы
        /// </summary>
        public List<DisciplineDto>? Disciplines { get; set; }
    }
}