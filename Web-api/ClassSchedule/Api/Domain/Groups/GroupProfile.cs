﻿using AutoMapper;
using DataAccessLayer.Models;

namespace Api.Domain.Groups
{
    public class GroupProfile : Profile
    {
        public GroupProfile()
        {
            CreateMap<GroupDto, Group>();
            CreateMap<Group, GroupDto>();
        }
    }
}
