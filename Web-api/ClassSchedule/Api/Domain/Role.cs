﻿namespace Api.Domain
{
    public enum Role
    {
        Student = 1,
        Teacher = 2,
    }
}
