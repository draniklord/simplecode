﻿using AutoMapper;
using DataAccessLayer.Models;

namespace Api.Domain.DirectionsOfStudy
{
    public class DirectionProfile : Profile
    {
        public DirectionProfile()
        {
            CreateMap<DirectionOfStudyDto, DirectionOfStudy>();
            CreateMap<DirectionOfStudy, DirectionOfStudyDto>();
        }
    }
}
