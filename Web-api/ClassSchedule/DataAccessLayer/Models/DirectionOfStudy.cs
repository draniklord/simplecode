﻿namespace DataAccessLayer.Models
{
    /// <summary>
    /// Направление подготовки
    /// </summary>
    public class DirectionOfStudy
    {
        /// <summary>
        /// Номер направления подготовки
        /// </summary>
        public int? DirectionOfStudyId { get; set; }

        /// <summary>
        /// Наименование направления
        /// </summary>
        public string? DirectionOfStudyName { get; set; }

        /// <summary>
        /// Группы данного направления
        /// </summary>
        public ICollection<Group>? Groups { get; set; }
        //public int? GroupId { get; set; }

        /// <summary>
        /// Дисциплины данного направления
        /// </summary>
        public ICollection<Discipline>? Disciplines { get; set; }
        //public int? DisciplineId { get; set; }

        /// <summary>
        /// Выпускающая кафедра данного направления
        /// </summary>
        public GraduatingDepartment? GraduatingDepartment { get; set; }
        public int? GraduatingDepartmentId { get; set; }
    }
}
