﻿using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer.Models
{
    /// <summary>
    /// Дисциплина
    /// </summary>
    public class Discipline
    {
        /// <summary>
        /// Номер дисциплины
        /// </summary>
        public int? DisciplineId { get; set; }

        /// <summary>
        /// Название дисциплины
        /// </summary>
        /// 
        public string? DisciplineName { get; set; }

        /// <summary>
        /// Группы по данной дисциплине
        /// </summary>
        public ICollection<Group>? Groups { get; set; }
        public int? GroupId { get; set; }

        /// <summary>
        /// Преподаватели данной дисциплины
        /// </summary>
        public ICollection<Teacher>? Teachers { get; set; }
        //public int? TeachersId { get; set; }
    }
}
