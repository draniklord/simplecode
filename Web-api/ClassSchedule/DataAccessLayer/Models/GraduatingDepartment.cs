﻿namespace DataAccessLayer.Models
{
    /// <summary>
    /// Выпускающая кафедра
    /// </summary>
    public class GraduatingDepartment
    {
        /// <summary>
        /// Номер кафедры, он же этаж
        /// </summary>
        public int? GraduatingDepartmentId { get; set; }

        /// <summary>
        /// Направления подготовки
        /// </summary>
        public ICollection<DirectionOfStudy>? DirectionsOfStudy { get; set; }
        //public int? DirectionOfStudyId { get; set; }

        /// <summary>
        /// Номера аудиториий
        /// </summary>
        public int? AudienceNumbers { get; set; }
    }
}