﻿namespace DataAccessLayer.Models
{
    public enum Role
    {
        Student = 1,
        Teacher = 2,
    }
}
