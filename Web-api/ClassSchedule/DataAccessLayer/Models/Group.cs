﻿namespace DataAccessLayer.Models
{
    /// <summary>
    /// Группа
    /// </summary>
    public class Group
    {
        /// <summary>
        /// Номер группы
        /// </summary>
        /// 
        public int? GroupId { get; set; }

        /// <summary>
        /// Название группы
        /// </summary>
        public string? GroupName { get; set; }

        //public int? StudentId { get; set; }
        /// <summary>
        /// Студенты в данной группе
        /// </summary>
        public ICollection<Student>? Student { get; set; }

        //public int? DisciplineId { get; set; }
        /// <summary>
        /// Дисциплины у данной группы
        /// </summary>
        public ICollection<Discipline>? Discipline { get; set; }
    }
}