﻿using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations
{
    public class GroupConfiguration : IEntityTypeConfiguration<Group>
    {
        public void Configure(EntityTypeBuilder<Group> builder)
        {
            builder.HasKey(group => group.GroupId);

            builder
                .HasMany(student => student.Student)
                .WithOne(group => group.Group)
                .HasForeignKey(key => key.StudentId);

            builder
                .HasMany(discipline => discipline.Discipline)
                .WithMany(group => group.Groups);
        }
    }
}
