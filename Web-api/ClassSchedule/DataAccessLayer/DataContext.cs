﻿using DataAccessLayer.Configurations;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer
{
    public sealed class DataContext : DbContext
    {        
        public DbSet<GraduatingDepartment>? GraduatingDepartments { get; set; }
        public DbSet<DirectionOfStudy>? DirectionOfStudies { get; set; }
        public DbSet<Discipline>? Disciplines { get; set; }
        public DbSet<Group>? Groups { get; set; }
        public DbSet<Student>? Students { get; set; }
        public DbSet<Teacher>? Teachers { get; set; }



        public DataContext(DbContextOptions options) : base(options)
        {
            //Database.EnsureDeleted();
            //Database.EnsureCreated();
            Database.Migrate();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Student>()
                .Property(x => x.Role)
                .HasConversion(
                    v => v.ToString(),
                    v => (Role)Enum.Parse(typeof(Role), v));

            modelBuilder
                .Entity<Teacher>()
                .Property(x => x.Role)
                .HasConversion(
                     v => v.ToString(),
                     v => (Role)Enum.Parse(typeof(Role), v));

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(GroupConfiguration).Assembly);
        }
    }
}
