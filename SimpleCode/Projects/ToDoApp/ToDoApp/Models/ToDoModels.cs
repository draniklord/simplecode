﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoApp.Models
{
    class ToDoModels: INotifyPropertyChanged
    {
        public DateTime CreationDate { get; set; } = DateTime.Now;

        private bool _isdone;
        private string _text;


        public bool IsDone
        {
            get { return _isdone; }
            set 
            {
                if (_isdone == value)
                {
                    return;
                }
                _isdone = value;
                OnPropertyChanged("IsDone");
            }
        }

        public string Text
        {
            get { return _text; }
            set 
            {
                if (_text == value) 
                {
                    return;
                }
                _text = value;
                OnPropertyChanged("Text");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propetryName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propetryName));
        }
    }
}
