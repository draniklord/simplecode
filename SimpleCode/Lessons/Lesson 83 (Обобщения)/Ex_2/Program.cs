﻿using System;
using System.Collections.Generic;

namespace Ex_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<int> list = new List<int>();
            list.Add(5);
            list.Add(7);
            list.Add(9);
            list.Add(45);

            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine(list[i]);
            }

            Console.WriteLine("\n");

            MyList<int> myList = new MyList<int>();
            myList.Add(45);
            myList.Add(9);
            myList.Add(7);
            myList.Add(5);

            for (int i = 0; i < myList.Count; i++)
            {
                Console.WriteLine(myList[i]);
            }
        }
    }
}