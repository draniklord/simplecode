﻿using System;

// Ключевое слово static

namespace Ex_1
{
    class MyClass
    {
        public int a;

        public static int b;
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            MyClass.b = 5;

            MyClass myClass1 = new MyClass();
            myClass1.a = 44;

            MyClass myClass2 = new MyClass();
            myClass2.a = 22;
        }
    }
}