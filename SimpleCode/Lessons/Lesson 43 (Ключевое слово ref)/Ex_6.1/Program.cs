﻿using System;

namespace Ex_5._1
{
    class Program
    {
        static ref int Foo(int[] numbers)
        {
            return ref numbers[0];
        }

        static void Main(string[] args)
        {
            int[] arr = { 2, 6, 1 };

            ref int b = ref Foo(arr);

            b = -5;

            Console.WriteLine(arr[0]);

            Console.ReadLine();
        }
    }
}