﻿using System;

namespace Ex_2
{
    class Program
    {
        static void Bar(int[] arr)
        {
            //arr[0] = 5;
            arr = null;
        }

        static void Main(string[] args)
        {
            int[] myArray = { 1, 4, 6 };

            Bar(myArray);

            Console.WriteLine(myArray[0]);

            Console.ReadLine();
        }
    }
}