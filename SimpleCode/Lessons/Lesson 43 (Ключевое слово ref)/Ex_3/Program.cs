﻿using System;

namespace Ex_2
{
    class MyClass
    {
        public int a;
        public double b;
        public float c;
    }

    class Program
    {
        static void Foo(MyClass myClass)
        {
            myClass.a = 5;
        }

        static void Main(string[] args)
        {
            MyClass myClass = new MyClass();

            Foo(myClass);

            Console.WriteLine(myClass.a);

            Console.ReadLine();
        }
    }
}