﻿using System;

namespace Ex_5._1
{
    class Program
    {
        static int Foo(int[] numbers)
        {
            return numbers[0];
        }

        static void Main(string[] args)
        {
            int[] arr = { 2, 6, 1 };

            int b = Foo(arr);

            b = -5;

            Console.WriteLine(arr[0]);

            Console.ReadLine();
        }
    }
}