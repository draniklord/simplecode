﻿using System;

namespace Ex_2
{
    class Program
    {
        static void Bar(int[] arr)
        {
            arr = new int[10];
        }

        static void Main(string[] args)
        {
            int[] myArray = { 1, 4, 6 };

            Bar(myArray);

            Console.WriteLine(myArray.Length);

            Console.ReadLine();
        }
    }
}