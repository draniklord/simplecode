﻿using System;

namespace Ex_2
{
    struct MyStruct
    {
        public int a;
        public double b;
        public float c;
    }

    class Program
    {
        static void Foo(ref MyStruct myStruct)
        {
            myStruct.a = 5;
        }

        static void Main(string[] args)
        {
            MyStruct myStruct = new MyStruct();

            Foo(ref myStruct);

            Console.WriteLine(myStruct.a);

            Console.ReadLine();
        }
    }
}