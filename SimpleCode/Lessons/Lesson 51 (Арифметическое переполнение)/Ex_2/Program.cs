﻿using System;

namespace Ex_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int a = int.MaxValue;
            a = a + 1; //переполнение через вернхюю границу
            Console.WriteLine(a);

            int b = int.MinValue;
            b = b - 1; //переполнение через нижнюю границу
            Console.WriteLine(b);
        }
    }
}