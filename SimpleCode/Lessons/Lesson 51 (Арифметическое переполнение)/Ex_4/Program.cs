﻿using System;

namespace Ex_4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            decimal a = decimal.MaxValue;
            decimal b = decimal.MaxValue;
            decimal c = unchecked(a + b); // Для операндов типа decimal арифметическое переполнение всегда вызывает исключение
        }
    }
}