﻿using System;

// АРИФМЕТИЧЕСКОЕ ПЕРЕПОЛНЕНИЕ
// checked unchecked c#

namespace Ex_1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            byte agression = 1;

            byte democracyModifier = 2;

            try
            {
                agression = checked((byte)(agression - democracyModifier)); // сужающее преобразование
                Console.WriteLine(agression);
            }
            catch (OverflowException)
            {
                Console.WriteLine("Ошибка переполнения!");
            }
        }
    }
}