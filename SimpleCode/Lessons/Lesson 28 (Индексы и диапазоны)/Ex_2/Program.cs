﻿using System;

namespace Ex_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] myArray = { 2, 10, 5, 6, 77, 89 };

            //вывести элемент под индексом (true - с конца, false - сначала)
            Index myIndex = new Index(3, false);
            Console.WriteLine(myArray[myIndex]);
            Console.WriteLine();

            //вывести числа из диапазона индексов
            Range myRange = new Range(1,4);

            int[] myArray2 = myArray[myRange];
            for (int i = 0; i < myArray2.Length; i++)
            {
                Console.Write(myArray2[i] + "\t");
            }
            Console.WriteLine("\n");
            //или
            Range myRange2 = ^4..^1;

            int[] myArray3 = myArray[myRange2];
            for (int i = 0; i < myArray3.Length; i++)
            {
                Console.Write(myArray3[i] + "\t");
            }
            Console.WriteLine();
            //или
            int[] myArray4 = myArray[^4..^1];
            for (int i = 0; i < myArray4.Length; i++)
            {
                Console.Write(myArray4[i] + "\t");
            }
        }
    }
}