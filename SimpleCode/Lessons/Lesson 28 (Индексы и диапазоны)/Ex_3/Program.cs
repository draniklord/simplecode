﻿using System;

namespace Ex_3
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = "Hello world !!! =)";

            Console.WriteLine(str[^2..]);
            Console.WriteLine(str[0..6]);
            Console.WriteLine(str[6..11]);
        }
    }
}