﻿using System;

namespace Ex_4
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] myArray = { 2, 8, 22, 53, 13, 55, 77, 123 };

            //извлечение второго элемента с конца
            Console.WriteLine(myArray[^2]);
            Console.WriteLine();

            //примеры извлечений диапазонов элементов из массива с выводом на консоль
            foreach (var item in myArray[..4])
            {
                Console.WriteLine($"{item} ");
            }
            Console.WriteLine();

            foreach (var item in myArray[4..])
            {
                Console.WriteLine($"{item} ");
            }
            Console.WriteLine();

            foreach (var item in myArray[5..7])
                Console.WriteLine($"{item} ");
            Console.WriteLine();
        }
    }
}