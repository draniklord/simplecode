﻿using System;

namespace Lesson_28
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] myArray = { 2, 10, 5, 6, 77, 89 };

            //вывести число массива с конца (значок ^)
            //с конца нумерация с 1
            Console.WriteLine(myArray[^1]);
            Console.WriteLine();

            //извлечь числа из диапазона
            int[] myArray2 = myArray[1..4]; //индексы
            for (int i = 0; i < myArray2.Length; i++)
            {
                Console.Write(myArray2[i] + "\t");
            }
            Console.WriteLine("\n");

            //извлечь диапазон с первого элемента
            int[] myArray3 = myArray[..4]; //индексы
            for (int i = 0; i < myArray3.Length; i++)
            {
                Console.Write(myArray3[i] + "\t");
            }
            Console.WriteLine("\n");

            //извлечь диапазон от определенного до последнего
            int[] myArray4 = myArray[2..]; //индексы
            for (int i = 0; i < myArray4.Length; i++)
            {
                Console.Write(myArray4[i] + "\t");
            }
            Console.WriteLine("\n");


            Index myIndex = 2;
            Console.WriteLine(myArray[myIndex]);

            Console.WriteLine($"value {myIndex.Value} isFromEnd {myIndex.IsFromEnd}");
            //value - индекс
            //isFromEnd - элемент с конца
        }
    }
}