﻿using System;

namespace Ex_5
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();

            int[,,,] myArray = new int[3, 2, 4, 5];

            for (int i = 0; i < myArray.GetLength(0); i++)
            {
                for (int j = 0; j < myArray.GetLength(1); j++)
                {
                    for (int k = 0; k < myArray.GetLength(2); k++)
                    {
                        for (int q = 0; q < myArray.GetLength(3); q++)
                        {
                            myArray[i, j, k, q] = random.Next(50);
                        }
                    }
                }
            }

            for (int i = 0; i < myArray.GetLength(0); i++)
            {
                Console.WriteLine("=== Book №: " + (i + 1) + " ===");
                for (int j = 0; j < myArray.GetLength(1); j++)
                {
                    Console.WriteLine("Page №: " + (j + 1));
                    for (int k = 0; k < myArray.GetLength(2); k++)
                    {
                        for (int q = 0; q < myArray.GetLength(3); q++)
                        {
                            Console.Write(myArray[i, j, k, q] + "\t");
                        }
                        Console.WriteLine();
                    }
                    Console.WriteLine();
                }
                Console.WriteLine();
            }

            Console.ReadLine();
        }
    }
}