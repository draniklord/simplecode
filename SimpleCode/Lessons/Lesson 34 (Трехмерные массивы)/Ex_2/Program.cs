﻿using System;

namespace Ex_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,,] myArray =
            {
                {
                    {2,3,5 },
                    {15,13,8 }
                },
                {
                    {6,13,56 },
                    {19,14,26 }
                },
                {
                    {613,32,132 },
                    {3,165,6 }
                }
            };

            Console.ReadLine();
        }
    }
}