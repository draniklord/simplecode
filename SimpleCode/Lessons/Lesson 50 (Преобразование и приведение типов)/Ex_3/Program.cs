﻿using System;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            int i1 = 455, i2 = 84500;
            decimal dec = 7.98845m;

            // Приводим два числа типа int
            // к типу short
            Console.WriteLine((short)i1);
            Console.WriteLine((short)i2);

            // Приводим число типа decimal
            // к типу int
            Console.WriteLine((int)dec);

            Console.ReadLine();
        }
    }
}