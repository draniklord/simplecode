﻿using System;

//ПРЕОБРАЗОВАНИЕ И ПРИВЕДЕНИЕ ТИПОВ В C#

namespace Lesson_50
{
    internal class Program
    {
        static void Foo(int a)
        {
            Console.WriteLine(a);
        }

        static void Main(string[] args)
        {
            float a = 5.5f; 

            Foo((int)a);

            byte b = (byte)a;

            bool c = Convert.ToBoolean(a);

            int result = (int)(a + b); // или (int)a + b;
        }
    }
}