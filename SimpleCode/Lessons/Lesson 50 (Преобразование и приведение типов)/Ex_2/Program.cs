﻿using System;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            short num1, num2;
            num1 = 10;
            num2 = 15;

            Console.WriteLine("{0} + {1} = {2}", num1, num2, Sum(num1, num2));
            Console.ReadLine();
        }

        static int Sum(int x, int y)
        {
            return x + y;
        }
    }
}