﻿using System;

namespace Ex_1
{
    partial class Person
    {
        public partial string GetFullName()
        {
            return FirstName + " " + LastName;
        }

        public void PrintFullName()
        {
            Console.WriteLine(GetFullName());
        }
    }
}