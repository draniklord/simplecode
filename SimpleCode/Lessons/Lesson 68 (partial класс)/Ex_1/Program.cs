﻿// Частичные (partial) типы, классы и методы

namespace Ex_1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Person person = new Person("Будкин", "Шамиль");
            person.PrintFullName();
        }
    }
}