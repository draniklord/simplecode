﻿using System;

// Явная реализация интерфейсов

namespace Ex_1
{
    interface IFirstInterface
    {
        void Action();
    }
    interface ISecondInterface
    {
        void Action();
    }

    class MyClass : IFirstInterface, ISecondInterface
    {
        public void Action()
        {
            Console.WriteLine("MyClass Action");
        }
    }

    class MyOtherClass : IFirstInterface, ISecondInterface
    {
        void IFirstInterface.Action()
        {
            Console.WriteLine("MyOtherClass IFirstInterface.Action");
        }
        void ISecondInterface.Action()
        {
            Console.WriteLine("MyOtherClass ISecondInterface.Action");
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            MyClass myClass = new MyClass();
            Foo(myClass);
            Bar(myClass);

            Console.WriteLine();

            MyOtherClass myOtherClass = new MyOtherClass();
            Foo(myOtherClass);
            Bar(myOtherClass);

            Console.WriteLine();

            //IFirstInterface firstInterface = myOtherClass;
            //firstInterface.Action();

            ((IFirstInterface)myOtherClass).Action();
            ((ISecondInterface)myOtherClass).Action();

            Console.WriteLine();

            if (myOtherClass is IFirstInterface firstInterface)
            {
                firstInterface.Action();
            }
        }

        static void Foo(IFirstInterface firstInterface)
        {
            firstInterface.Action();
        }
        static void Bar(ISecondInterface secondInterface)
        {
            secondInterface.Action();
        }
    }
}