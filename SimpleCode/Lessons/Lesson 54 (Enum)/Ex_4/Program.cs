﻿using System;

namespace Ex_4
{
    enum Color
    {
        White,
        Red,
        Green,
        Blue,
        Orange
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            var values = Enum.GetValues(typeof(Color));

            foreach (var item in values)
            {
                Console.WriteLine(item);
            }
        }
    }
}