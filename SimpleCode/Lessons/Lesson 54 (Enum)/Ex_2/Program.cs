﻿using System;

namespace Ex_2
{
    internal class Program
    {
        enum DayOfWeek : byte // по умолчанию int
        {
            Monday = 1,
            Tuesday,
            Wednesday,
            Thursday,
            Friday,
            Saturday,
            Sunday
        }
        
        static void Main(string[] args)
        {
            DayOfWeek dayOfWeek = DayOfWeek.Monday;

            Console.WriteLine(Enum.GetUnderlyingType(typeof(DayOfWeek))); // получаем тип данных

            Console.WriteLine(dayOfWeek);

            Console.WriteLine((int)dayOfWeek); // приводим dayOfWeek к int

            Console.WriteLine((DayOfWeek)3); // приводим int к DayOfWeek

            DayOfWeek nextDay = GetNextDay(dayOfWeek);
            Console.WriteLine(nextDay);
        }
      
        /// <summary>
        /// Получаем следующий день
        /// </summary>
        /// <param name="day"></param>
        /// <returns></returns>
        static DayOfWeek GetNextDay(DayOfWeek day)
        {
            if (day < DayOfWeek.Sunday)
                return day + 1;

            return DayOfWeek.Monday;
        }
    }
}