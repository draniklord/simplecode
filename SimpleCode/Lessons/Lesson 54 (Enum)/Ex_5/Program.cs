﻿using System;

namespace Ex_5
{
    internal class Program
    {
        enum Color
        {
            White,
            Red,
            Green,
            Blue,
            Orange
        }
        static void Main(string[] args)
        {
            string str = Console.ReadLine();
            Color color = (Color)Enum.Parse(typeof(Color), str, ignoreCase: true);
            Console.WriteLine(color);

            switch (color)
            {
                case Color.White:
                    break;
                case Color.Red:
                    break;
                case Color.Green:
                    break;
                case Color.Blue:
                    break;
                case Color.Orange:
                    break;
                default:
                    break;
            }
        }
    }
}