﻿using System;

namespace Lesson_33
{
    class Program
    {
        static void Main(string[] args)
        {
            int[][] myArray = new int[3][];
            myArray[0] = new int[5];
            myArray[1] = new int[2];
            myArray[2] = new int[10];

            myArray[0][3] = 55;

            int[] arr = myArray[0];

            Console.WriteLine(myArray[0][3]);
            Console.WriteLine();

            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write(arr[i] + "\t");
            }

            Console.ReadLine();
        }
    }
}