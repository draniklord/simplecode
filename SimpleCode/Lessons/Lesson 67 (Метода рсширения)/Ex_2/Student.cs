﻿namespace Ex_2
{
    sealed class Student
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}