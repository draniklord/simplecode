﻿using Ex_2_Extension;

namespace Ex_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Student student = new Student() { FirstName = "Данька", LastName = "Заводилов" };

            string fullName = student.GetFullName();
            System.Console.WriteLine(fullName);
        }
    }
}