﻿using Ex_2;

namespace Ex_2_Extension
{
    static class MyExtension
    {
        public static string GetFullName(this Student student)
        {
            return student.LastName + " " + student.FirstName;
        }
    }
}