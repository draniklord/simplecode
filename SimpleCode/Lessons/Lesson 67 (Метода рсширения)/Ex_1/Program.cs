﻿using System;
using Ex_1_MyExtension;

// Методы расширения 

namespace Ex_1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            DateTime currentDateTime = DateTime.Now;
            currentDateTime.Print();

            DateTime.Now.Print();

            Console.WriteLine(currentDateTime.IsDayOfWeek(DayOfWeek.Monday));
        }
    }
}