﻿using System;

// Методы

namespace Ex_1
{
    internal class Program
    {
        static Student GetStudent()
        {
            Student student = new Student();

            student.firstName = "Геныч";
            student.lastName = "Пукович";
            student.middleName = "Сюрикенович";
            student.age = 15;
            student.id = Guid.NewGuid();
            student.group = "ЭА-191";

            return student;
        }
        
        static void Main(string[] args)
        {
            var firtstStudent = GetStudent();

            firtstStudent.Print();

            string fullName = firtstStudent.GetFullName();
            Console.WriteLine();
            Console.WriteLine(fullName);
        }      
    }
}