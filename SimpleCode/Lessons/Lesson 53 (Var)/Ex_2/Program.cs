﻿using System;
using System.Linq;

namespace Ex_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int[] numders = { 1, 5, 344, 40, 1, 24, 6, 9, 11 };

            var result = from i in numders where i > 15 select i;

            foreach (var item in result)
            {
                Console.Write($"{item}\t");
            }

            Console.WriteLine();

            Console.WriteLine($"Тип данных result {result.GetType()}");
        }
    }
}