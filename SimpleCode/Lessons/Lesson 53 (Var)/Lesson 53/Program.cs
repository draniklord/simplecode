﻿using System;

// Ключевое слово var

namespace Lesson_53
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var a = 5.5;
            var b = "Hello World!";
            var c = new float[10];
            var d = new { Name = "Biba", Age = 25 };

            Console.WriteLine(a.GetTypeCode());
            Console.WriteLine(b.GetTypeCode());
            Console.WriteLine(c.GetType());
            Console.WriteLine(d.GetType());

            b = null;
        }
    }
}