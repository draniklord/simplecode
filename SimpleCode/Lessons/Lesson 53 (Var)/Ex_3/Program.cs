﻿using System;

namespace Ex_3
{
    internal class Program
    {
        static int Sum(int a, int b)
        {
            var result = a + b;
            return result;
        }

        static void Main(string[] args)
        {
            var t = Sum(5, 12);

            Console.WriteLine(t);
            Console.WriteLine(t.GetType());
        }
    }
}