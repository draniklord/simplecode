﻿using System;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Пример возведения числа в несколько степеней
            byte l = 2, i = 0;
            int result = 1;

            while (i < 10)
            {
                i++;
                result *= l;
                Console.WriteLine("{0} в степени {1} равно {2}", l, i, result);
            }

            Console.ReadLine();
        }
    }
}