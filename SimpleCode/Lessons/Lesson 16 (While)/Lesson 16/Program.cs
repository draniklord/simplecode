﻿using System;

namespace Lesson_16
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 0;
            int limit = int.Parse(Console.ReadLine());

            while (a < limit)
            {
                a++;
                Console.WriteLine(a);
            }
            Console.ReadKey();
        }
    }
}