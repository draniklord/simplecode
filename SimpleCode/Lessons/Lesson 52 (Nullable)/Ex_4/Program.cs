﻿using System;

namespace Ex_4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Nullable<DateTime> dateTime = DateTime.Now;

            bool a = dateTime.HasValue;

            Console.WriteLine(dateTime);
            Console.WriteLine(a);
        }
    }
}