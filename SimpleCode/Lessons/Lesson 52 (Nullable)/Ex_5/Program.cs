﻿using System;

namespace Ex_5
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int a = 5;
            int? b = 28; //если null, то сложения не будет
            int? result = a + b;

            Console.WriteLine(result);
            Console.WriteLine(a == b);
            Console.WriteLine(a > b);
            Console.WriteLine(a < b);
        }
    }
}