﻿using System;

namespace Ex_3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int? i = 2;

            Console.WriteLine(i == null);

            Console.WriteLine(i.HasValue);

            Console.WriteLine(i.GetValueOrDefault());

            Console.WriteLine(i.GetValueOrDefault(3));

            Console.WriteLine(i ?? 5);

            Console.WriteLine(i);

            Console.WriteLine(i.Value);
        }
    }
}