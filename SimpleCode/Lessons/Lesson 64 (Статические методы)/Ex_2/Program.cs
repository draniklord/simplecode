﻿using System;

namespace Ex_2
{
    class MyClass
    {
        public MyClass()
        {
            counter++;
        }

        private static int counter;

        public static int Counter
        {
            get { return counter; }
            private set { counter = value; }
        }

        public int ObjectsCount
        {
            get { return counter; }
        }

        public static int GetCounter()
        {
            return counter;
        }

        public int GetObjectsCount()
        {
            return counter;
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        { 
            MyClass myClass1 = new MyClass();
            Console.WriteLine(myClass1.ObjectsCount);

            MyClass myClass2 = new MyClass();

            Console.WriteLine(myClass1.GetObjectsCount());

            MyClass myClass3 = new MyClass();

            Console.WriteLine(MyClass.Counter);
            Console.WriteLine(MyClass.GetCounter());
        }
    }
}