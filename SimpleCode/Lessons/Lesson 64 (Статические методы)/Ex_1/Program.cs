﻿using System;

// Статические методы

namespace Ex_1
{
    class MyClass
    {
        public static void Foo()
        {
            Console.WriteLine("Вызван метод Foo");
            a = 5;
            Console.WriteLine(a);
        }

        public void Bar()
        {
            Console.WriteLine("Вызван метод Bar");
            Console.WriteLine(a);

            Foo();
        }

        private static int a;

        public static int A
        {
            get { return a; }
            set { a = value; }
        }

        public static int B { get; set; }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            MyClass myClass = new MyClass();
            myClass.Bar();

            Console.WriteLine();

            MyClass.Foo();

            MyClass.A = 30;
            MyClass.B = 5;
        }       
    }
}