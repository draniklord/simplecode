﻿using System;

namespace Lesson_45
{
    class Program
    {
        static void Foo(in int value)
        {
            Console.WriteLine(value);
            //value = 555; //изменить входящий параметр не можем
        }

        static void Main(string[] args)
        {
            int a = 5;

            Foo(a);

            Console.ReadLine();
        }
    }
}