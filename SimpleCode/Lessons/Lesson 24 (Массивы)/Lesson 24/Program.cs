﻿using System;

namespace Lesson_24
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] myArray;

            myArray = new int[5];

            myArray[0] = 10;
            myArray[1] = 3;

            int a = myArray[0];

            Console.WriteLine(a);
            Console.WriteLine(myArray[1]);
            Console.WriteLine(myArray.Length);

            Console.ReadLine();
        }
    }
}