﻿using System;

// Наследование интерфейсов

namespace Ex_1
{
    interface IWeapon
    {
        void Fire();
    }
    interface IThrowingWeapon : IWeapon
    {
        void Throw();
    }

    class Gun : IWeapon
    {
        public void Fire()
        {
            Console.WriteLine($"{GetType().Name}: Бах!");
        }
    }
    class LaserGun : IWeapon
    {
        public void Fire()
        {
            Console.WriteLine($"{GetType().Name}: Пию пию!");
        }
    }

    class Khife : IThrowingWeapon
    {
        public void Fire()
        {
            Console.WriteLine($"{GetType().Name}: Чик чик!");
        }

        public void Throw()
        {
            Console.WriteLine($"{GetType().Name}: Я лечу!");
        }
    }

    class Player
    {
        public void Fire(IWeapon weapon)
        {
            weapon.Fire();
        }
        public void Throw(IThrowingWeapon throwingWeapon)
        {
            throwingWeapon.Throw();
        } 
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            Player player = new Player();
            IWeapon[] inventory1 = { new Gun(), new LaserGun(), new Khife() };
            IThrowingWeapon[] inventory2 = { new Khife() };

            foreach (var item in inventory1)
            {
                player.Fire(item);
                Console.WriteLine();
            }
            foreach (var item in inventory2)
            {
                player.Throw(item);
                Console.WriteLine();
            }
        }
    }
}