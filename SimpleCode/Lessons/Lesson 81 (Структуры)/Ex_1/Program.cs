﻿using System;

// Структуры 

namespace Ex_1
{
    public class ClassPoint
    {
        public int X { get; set; }
        public int Y { get; set; }
        public void Print()
        {
            Console.WriteLine($"X: {X}\tY: {Y}");
        }
    }
    public struct StructPoint
    {
        public int X { get; set; }
        public int Y { get; set; }
        public void Print()
        {
            Console.WriteLine($"X: {X}\tY: {Y}");
        }
    }

    internal class Program
    {
        static void Foo(ClassPoint classPoint)
        {
            classPoint.X++;
            classPoint.Y++;
        }

        static void Bar(StructPoint structPoint)
        {
            structPoint.X++;
            structPoint.Y++;
        }

        static void Main(string[] args)
        {
            ClassPoint classPoint = new ClassPoint();
            StructPoint structPoint = new StructPoint();

            Foo(classPoint);
            Bar(structPoint);

            classPoint.Print();
            structPoint.Print();

            ClassPoint classPoint1 = new ClassPoint { X = 2, Y = 3 };
            ClassPoint classPoint2 = new ClassPoint { X = 2, Y = 3};

            bool classAreaEqual = classPoint1.Equals(classPoint2);

            StructPoint structPoint1 = new StructPoint { X = 2, Y = 3 };
            StructPoint structPoint2 = new StructPoint { X = 2, Y = 3 };

            bool structAreaEqual = structPoint1.Equals(structPoint2);

            Console.WriteLine($"Class Equal: {classAreaEqual}\tStruct Equal: {structAreaEqual}");
        }
    }
}