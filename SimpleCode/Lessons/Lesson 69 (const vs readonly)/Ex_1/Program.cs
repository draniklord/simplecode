﻿using System;

// const и readonly

namespace Ex_1
{
    class MyClass
    {
        public const double PI = 3.14;
        private const string MY_ERROR = "some error";

        private readonly int _a;
        private static readonly int _b;

        static MyClass()
        {
            _b = 3;
        }

        public MyClass(int a)
        {
            _a = a;
        }
        public static void Foo()
        {
            Console.WriteLine(PI);
            Console.WriteLine(MY_ERROR);
            Console.WriteLine(_b);
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            MyClass myClass = new MyClass(4);

            double pI = MyClass.PI;

            MyClass.Foo();
        }
    }
}