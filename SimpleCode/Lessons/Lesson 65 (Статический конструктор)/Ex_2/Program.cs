﻿using System;

namespace Ex_2
{
    class DbRepository
    {
        private static string connectionString; //= "local DB";

        static DbRepository()
        {
            ConfigurationManager configurationManager = new ConfigurationManager();
            connectionString = configurationManager.GetConnectionString();
        }

        public void GetData()
        {
            Console.WriteLine("Использую: " + connectionString);
        }
    }

    class ConfigurationManager
    {
        public string GetConnectionString()
        {
            return "local DB";
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            DbRepository dbRepository = new DbRepository(); 

            dbRepository.GetData();
        }
    }
}