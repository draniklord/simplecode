﻿using System;

// Статический конструктор класса

namespace Ex_1
{
    class MyClass
    {
        static int a;

        public MyClass()
        {
            Console.WriteLine("Конструктор");
        }

        static MyClass()
        {
            Console.WriteLine("Статический конструктор");

            a = 5;
            Bar();
        }

        public static void Foo()
        {
            Console.WriteLine("Foo");
        }

        public static void Bar()
        {

        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            MyClass.Foo();

            new MyClass();
            new MyClass();
            new MyClass();
        }
    }
}