﻿namespace Ex_2.Models
{
    class Program
    {
        static void Main(string[] args)
        {
            Teacher teacher = new Teacher { FirstName = "Piter", LastName = "Parker"};
            Student student = new Student { FirstName = "Maxim", LastName = "Tkachyk"};
            Person[] people = { teacher, student };

            PrintPersons(people);
        }

        static void PrintPersons(Person[] people)
        {
            foreach (var person in people)
            {
                person.PrintFullName();
            }
        }
    }
}