﻿using System;

namespace Ex_2.Models
{
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public void PrintFullName()
        {
            Console.WriteLine($"Фамилия: {LastName} \t Имя: {FirstName}");
        }
    }
}