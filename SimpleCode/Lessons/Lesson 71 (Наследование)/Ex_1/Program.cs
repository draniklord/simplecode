﻿using System;

// Наследование

namespace Ex_1
{
    class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public void PrintName()
        {
            Console.WriteLine($"Меня зовут {FirstName}!");
        }
    }

    class Student : Person
    {
        public void Learn()
        {
            Console.WriteLine("Я учусь!");
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            //Person person = new Person { FirstName = "Key", LastName = "Altos" };
            //person.PrintName();

            Student student = new Student { FirstName = "Key", LastName = "Altos" };
            student.PrintName();
            student.Learn();

            //Person person = new Student { FirstName = "Key", LastName = "Altos" };
            //person.PrintName();
            ////person.Learn();

            PrintFullName(student);
        }

        static void PrintFullName(Person person)
        {
            Console.WriteLine($"Фамилия: {person.LastName}\t Имя: {person.FirstName}");
        }
    }
}