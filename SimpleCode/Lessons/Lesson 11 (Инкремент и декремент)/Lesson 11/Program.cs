﻿using System;

namespace Lesson_11
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 5, b = 5, c = 5, d = 5;
            a = ++a * a; // => a = 36
            b = b++ * b; // => b = 30
            c = --c * c; // => c = 16
            d = d-- * d; // => d = 20

            Console.WriteLine(a);
            Console.WriteLine(b);
            Console.WriteLine(c);
            Console.WriteLine(d);

            Console.ReadLine();
        }
    }
}