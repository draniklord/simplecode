﻿using System;

namespace Ex_1
{
    class Gun
    {
        private bool IsLoaded;

        private void Reload()
        {
            Console.WriteLine("Заряжаю...");

            IsLoaded = true;

            Console.WriteLine("Заряжено!");
        }
        public void Shoot()
        {
            if (!IsLoaded)
            {
                Console.WriteLine("Орудие не заряжено!");
                Reload();
            }
            Console.WriteLine("Пиф-Паф\n");
            IsLoaded = false;
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            Gun gun = new Gun();
            gun.Shoot();
        }
    }
}