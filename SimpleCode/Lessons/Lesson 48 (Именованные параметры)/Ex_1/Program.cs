﻿using System;

namespace Ex_1
{
    class Program
    {
        static int Sum(int a, int b, bool enableLogging = false)
        {
            int result = a + b;

            if (enableLogging)
            {
                Console.WriteLine("Значение переменной a: " + a);
                Console.WriteLine("Значение переменной b: " + b);
                Console.WriteLine("Результат сложения: " + result);
            }

            return result;
        }

        static void Main(string[] args)
        {
            Sum(b: 5, a: 2);

            int firstValue = 19;
            int secondValue = 53;

            Sum(b: firstValue, a: secondValue, enableLogging: true);

            int result = Sum(firstValue, secondValue, enableLogging: true);
        }
    }
}