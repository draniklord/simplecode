﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_25
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = new int[] { 5, 6, 3 };
            int[] myArray = new [] { 5, 8, 6, 7 };
            int[] myArray2 = { 4, 2, 8, 2 };

            int[] myArray3 = Enumerable.Repeat(5, 10).ToArray();

            int[] myArray4 = Enumerable.Range(5, 10).ToArray();


            Console.ReadLine();
        }
    }
}