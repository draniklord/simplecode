﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Синтаксис инициализации массива с использованием
            // ключевого слова new
            int[] myArr = new int[] { 10, 20, 30, 40, 50 };

            // Синтаксис инициализации массива без использования
            // ключевого слова new
            string[] info = { "Фамилия", "Имя", "Отчество" };

            // Используем ключевое слово new и желаемый размер
            char[] symbol = new char[4] { 'X', 'Y', 'Z', 'M' };
        }
    }
}
