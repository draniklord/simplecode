﻿using System;

// Свойства get и set

namespace Ex_1
{
    class Point
    {
        private int x;

        public void SetX(int x)
        {
            this.x = x;
        }
        public int GetX()
        {
            return x;
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            Point point = new Point();

            point.SetX(15);
            int x = point.GetX();

            Console.WriteLine(x);
        }
    }
}