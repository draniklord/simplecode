﻿using System;

namespace Set
{
    class Point
    {
        private int y;
        public int Y
        {
            set
            {
                if (value < 1)
                {
                    y = 1;
                    return;
                }
                if (value > 5)
                {
                    y = 5;
                    return;
                }

                y = value;
            }
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            Point point = new Point();

            point.Y = 10;
            //int y = point.Y; // нельзя использовать, т.к нет get
            //Console.WriteLine(y);
        }
    }
}