﻿using System;

namespace Get
{
    class Point
    {
        private int y = 50;
        public int Y
        {
            get
            {
                return y;
            }
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            Point point = new Point();

            //point.Y = 10; // нельзя использовать, т.к нет set
            int y = point.Y;
            Console.WriteLine(y);
        }
    }
}