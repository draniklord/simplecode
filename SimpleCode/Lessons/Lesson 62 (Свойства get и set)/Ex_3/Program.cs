﻿using System;

// Автоматические свойства

namespace Ex_3
{
    class Point
    {
        private int x;
        public void SetX(int x)
        {
            if (x < 1)
            {
                this.x = 1;
                return;
            }
            if (x > 5)
            {
                this.x = 5;
                return;
            }

            this.x = x;
        }
        public int GetX()
        {
            return x;
        }

        private int y;
        public int Y
        {
            get
            {
                return y;
            }
            set
            {
                if (value < 1)
                {
                    y = 1;
                    return;
                }
                if (value > 50)
                {
                    y = 50;
                    return;
                }

                y = value;
            }
        }

        public int Z { get; set; }

        // Возможные использования 
        // public int Z { set; }
        // public int Z { get; }
        // public int Z { get; private set; }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            Point point = new Point();

            point.SetX(17);
            int x = point.GetX();
            Console.WriteLine(x);

            point.Y = 12;
            int y = point.Y;
            Console.WriteLine(y);

            point.Z = 92;
            int z = point.Z;
            Console.WriteLine(z);
        }
    }
}