﻿using System;

namespace Ex_2_1
{
    class Point
    {
        public Point()
        {
            Y = 4;
        }

        private int y;
        public int Y
        {
            get
            {
                return y;
            }
            private set
            {
                if (value < 1)
                {
                    y = 1;
                    return;
                }
                if (value > 5)
                {
                    y = 5;
                    return;
                }

                y = value;
            }
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            Point point = new Point();

            //point.Y = 10; // нельзя ничего присвоить, т.к у set модификатор private
            int y = point.Y;
            Console.WriteLine(y);
        }
    }
}