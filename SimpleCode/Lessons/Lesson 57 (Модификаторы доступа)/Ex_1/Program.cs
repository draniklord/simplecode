﻿using System;
using System.Reflection;

//Модификаторы доступа

namespace Ex_1
{
    class Point
    {
        int z = 3;

        public int x = 1;

        private int y = 44; 

        private void PrintX()
        {
            Console.WriteLine($"X: {x}");
        }

        public void PrintY()
        {
            Console.WriteLine($"Y: {y}");
        }

        public void PrintPoint()
        { 
            PrintX();
            PrintY();
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            Point point = new Point();

            point.x = 2;

            point.PrintY();

            point.PrintPoint();

            var type = typeof(Point).GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);

            foreach (var item in type)
            {
                Console.WriteLine($"{item.Name}\t IsPrivate: {item.IsPrivate}\t IsPublic: {item.IsPublic}");
            }
        }
    }
}