﻿using System;
using System.Linq;

namespace Ex_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] myArray = { 111, 10, 4, 111, 49, 64, 77, 4, 42, 5 };

            //первый элемент меньше 70
            int result_1 = Array.Find(myArray, i => i < 70);
            Console.WriteLine(result_1);
            Console.WriteLine();
            //или
            int result_7 = myArray.Where(i => i < 70).First(); //или FirstOrDefault(); если таких элементов не окажется

            //последний элемент меньше 70
            int result_2 = Array.FindLast(myArray, i => i < 70);
            Console.WriteLine(result_2);
            Console.WriteLine();

            //все элементы меньше 70
            int[] result_3 = Array.FindAll(myArray, i => i < 70);
            for (int i = 0; i < result_3.Length; i++)
            {
                Console.Write(result_3[i] + "\t");
            }
            Console.WriteLine("\n");
            //или
            int[] result_6 = myArray.Where(i => i < 70).ToArray();

            //под каким индексом число с начала
            int result_4 = Array.FindIndex(myArray, i => i == 111);
            Console.WriteLine(result_4);
            
            //под каким индексом число с конца
            int result_5 = Array.FindLastIndex(myArray, i => i == 111);
            Console.WriteLine(result_5);

            //в обратном порядке
            Array.Reverse(myArray);

            Console.ReadLine();
        }
    }
}