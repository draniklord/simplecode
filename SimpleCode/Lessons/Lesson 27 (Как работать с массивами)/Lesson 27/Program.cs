﻿using System;
using System.Linq;

namespace Lesson_27
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] myArray = { 111, 10, 4, 111, 49, 64, 77, 4, 42, 5 };

            int result_1 = myArray.Min();
            int result_2 = myArray.Max();

            //минимальный элемент массива
            Console.WriteLine(myArray.Min());
            Console.WriteLine(result_1);
            Console.WriteLine();

            //максимальный элемент массива
            Console.WriteLine(myArray.Max());
            Console.WriteLine(result_2);
            Console.WriteLine();

            //сумма всех элементов массива
            Console.WriteLine(myArray.Sum());
            Console.WriteLine();

            //сумма четных элементов массива
            Console.WriteLine(myArray.Where(i => i % 2 == 0).Sum());
            Console.WriteLine();

            //самое маленькое нечетное число
            Console.WriteLine(myArray.Where(i => i % 2 != 0).Min());
            Console.WriteLine();

            //выбор уникальных элементов массива (без повторяющихся)
            int[] resultArray_1 = myArray.Distinct().ToArray();
            for (int i = 0; i < resultArray_1.Length; i++)
            {
                Console.Write(resultArray_1[i] + "\t");
            }
            Console.WriteLine("\n");

            //сортировка в порядке увеличения
            int[] resultArray_2 = myArray.OrderBy(i => i).ToArray();
            for (int i = 0; i < resultArray_2.Length; i++)
            {
                Console.Write(resultArray_2[i] + "\t");
            }
            Console.WriteLine("\n");
            //или
            Array.Sort(myArray);

            //сортировка в порядке уменьшения
            int[] resultArray_3 = myArray.OrderByDescending(i => i).ToArray();
            for (int i = 0; i < resultArray_3.Length; i++)
            {
                Console.Write(resultArray_3[i] + "\t");
            }
            Console.WriteLine("\n");

            Console.WriteLine();

            Console.ReadLine();
        }
    }
}