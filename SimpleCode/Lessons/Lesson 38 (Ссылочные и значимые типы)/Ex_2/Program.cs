﻿using System;

namespace Ex_2
{
    class Program
    {
        static void Bar(int[] arr)
        {
            arr[0] = 5;
        }

        static void Main(string[] args)
        {
            int[] a = new int[1];

            a[0] = 1;

            Bar(a);

            Console.WriteLine(a[0]);

            Console.ReadLine();
        }
    }
}