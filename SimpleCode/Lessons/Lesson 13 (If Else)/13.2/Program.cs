﻿using System;

namespace _13._2
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = int.Parse(Console.ReadLine());

            if (a > 5)
            {
                Console.WriteLine("Введенное число больше 5");
            }
            else if (a < 5)
            {
                Console.WriteLine("Введенное число меньше 5");
            }
            else
            {
                Console.WriteLine("Введенное число равно 5");
            }

            Console.ReadLine();
        }
    }
}