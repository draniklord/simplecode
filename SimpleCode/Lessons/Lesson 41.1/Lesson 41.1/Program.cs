﻿using System;

namespace Lesson_41._1
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = null;

            str ??= string.Empty;

            Console.WriteLine("Количество символов в строке: " + str.Length);
        }
    }
}