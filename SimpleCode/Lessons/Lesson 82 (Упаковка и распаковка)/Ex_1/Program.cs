﻿using System;

// Упаковка и распаковка (boxing и unboxing)

namespace Ex_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 1;
            object b = a;
            int c = (int)b;
            //decimal d = (decimal)b; //InvalidCastException
            
            Console.WriteLine(c);
            Console.WriteLine(a.GetType());
            Console.WriteLine(b.GetType());
            Console.WriteLine(c.GetType());
        }
    }
}