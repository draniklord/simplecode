﻿using System;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;
using Ex_1;

namespace Benchmark
{
    [MemoryDiagnoser]
    public class Benchmark_1
    {
        [Benchmark]
        public void NoBoxing()
        {
            int res = 0;
            int a = 1;
            res += a;
        }
        [Benchmark]
        public void Boxing()
        {
            int res = 0;
            object a = 1;
            res += (int)a;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //BenchmarkRunner.Run<Benchmark_1>();
            BenchmarkSwitcher.FromAssembly(typeof(Program).Assembly).Run(args);
        }
    }
}