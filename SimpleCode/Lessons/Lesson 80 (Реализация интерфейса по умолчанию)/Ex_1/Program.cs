﻿// Реализация интерфейса по умолчанию в C#8.0

namespace Ex_1
{
    class Program
    {
        static void Main(string[] args)
        {
            ILogger consoleLogger = new ConsoleLogger();
            consoleLogger.Foo();
            consoleLogger.Log(LogLevel.Debug, "some event");
            consoleLogger.Log(LogLevel.Warning, "some warning");
            consoleLogger.Log(LogLevel.Fatal, "some fatal error");
            consoleLogger.LogError("fatal error");
        }
    }
}