﻿using System;

namespace Ex_1
{
    public interface ILogger
    {
        void Log(LogLevel level, string message);
        void LogError(string message)
        {
            Log(LogLevel.Error, message);
        }

        //void Foo()
        //{
        //    System.Console.WriteLine("Test");
        //}
    }
}