﻿using System;

// Полиморфизм

namespace Lesson_75
{
    class Car
    {
        protected virtual void StartEngine()
        {
            Console.WriteLine("Двигатель запущен!");
        }
        public virtual void Drive()
        {
            StartEngine();
            Console.WriteLine("Я машина, я еду!");
        }
    }

    class SportCar : Car
    {
        protected override void StartEngine()
        {
            Console.WriteLine("V8 работает!");
        }
        public override void Drive()
        {
            StartEngine();
            Console.WriteLine("Я еду очень быстро!");
        }
    }

    class Person
    {
        public void Drive(Car car)
        {
            car.Drive();
        }

    }

    internal class Program
    {
        static void Main(string[] args)
        {
            Person person = new Person();
            person.Drive(new Car());
            person.Drive(new SportCar());
        }
    }
}