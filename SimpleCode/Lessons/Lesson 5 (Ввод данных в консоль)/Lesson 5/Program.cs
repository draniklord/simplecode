﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string data; //объявление переменной
            data = Console.ReadLine(); //присвоение значение переменной из консоли

            Console.WriteLine($"Привет {data}"); //вывод переменной в консоль
            Console.WriteLine("Привет " + data); //вывод переменной в консоль (конкатенация)

            Console.ReadKey();
        }
    }
}