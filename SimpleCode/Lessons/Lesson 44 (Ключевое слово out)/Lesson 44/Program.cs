﻿using System;

namespace Lesson_44
{
    class Program
    {
        static void Foo(ref int value)
        {
            Console.WriteLine(value);
        }

        static void Bar(out int value)
        {
            value = 5; //обязательно присваивать значение
            Console.WriteLine(value);
        }

        static void Main(string[] args)
        {
            int a = 10; //обязательно присваивать значение для ref

            Foo(ref a); 

            Bar(out a);

            Console.ReadLine();
        }
    }
}