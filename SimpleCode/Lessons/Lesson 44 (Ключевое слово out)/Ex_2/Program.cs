﻿using System;

namespace Ex_2
{
    class Program
    {
        static void Bar(out int value)
        {
            value = 5;
        }

        static void Main(string[] args)
        {
            Bar(out int a);

            Console.WriteLine(a);

            Console.ReadLine();
        }
    }
}