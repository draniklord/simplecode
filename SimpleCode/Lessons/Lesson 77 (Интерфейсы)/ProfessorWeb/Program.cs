﻿using System;

namespace ConsoleApplication1
{
    // Создаем два интерфейса, описывающих абстрактные методы 
    // арифметических операций и операций Sqrt и Sqr
    public interface IArOperation
    {
        // Определяем набор абстрактных методов
        int Sum();
        int Otr();
        int Prz();
        int Del();
    }

    public interface ISqrSqrt
    {
        int Sqr(int x);
        int Sqrt(int x);
    }

    // Данный класс реализует интерфейс IArOperation
    class A : IArOperation
    {
        int My_x, My_y;

        public int x
        {
            set { My_x = value; }
            get { return My_x; }
        }

        public int y
        {
            set { My_y = value; }
            get { return My_y; }
        }

        public A() { }
        public A(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        // Реализуем методы интерфейса
        public virtual int Sum()
        {
            return x + y;
        }

        public int Otr()
        {
            return x - y;
        }

        public int Prz()
        {
            return x * y;
        }

        public int Del()
        {
            return x / y;
        }

        // В данном классе так же можно реализовать собственные методы
        public virtual void rewrite()
        {
            Console.WriteLine("Переменная x: {0}\nПеременная y: {1}", x, y);
        }
    }

    // Данный класс унаследован от класса А, но при этом в нем не нужно
    // заново реализовывать интерфейс, но при этом можно переопределить
    // некоторые его методы
    class Aa : A
    {
        public int z;

        public Aa(int z, int x, int y)
            : base(x, y)
        {
            this.z = z;
        }

        // Переопределим метод Sum
        public override int Sum()
        {
            return base.x + base.y + z;
        }

        public override void rewrite()
        {
            base.rewrite();
            Console.WriteLine("Переменная z: " + z);
        }
    }

    // Данный класс унаследован от класса А, и при этом
    // реализует интерфейс ISqrSqrt
    class Ab : A, ISqrSqrt
    {
        public int Sqr(int x)
        {
            return x * x;
        }

        public int Sqrt(int x)
        {
            return (int)Math.Sqrt((double)(x));
        }
    }

    class Program
    {
        static void Main()
        {
            A obj1 = new A(x: 10, y: 12);
            Console.WriteLine("obj1: ");
            obj1.rewrite();
            Console.WriteLine("{0} + {1} = {2}", obj1.x, obj1.y, obj1.Sum());
            Console.WriteLine("{0} * {1} = {2}", obj1.x, obj1.y, obj1.Prz());
            Aa obj2 = new Aa(z: -3, x: 10, y: 14);
            Console.WriteLine("\nobj2: ");
            obj2.rewrite();
            Console.WriteLine("{0} + {1} + {3} = {2}", obj2.x, obj2.y, obj2.Sum(), obj2.z);

            Console.ReadLine();
        }
    }
}