﻿using System;

namespace Ex_4
{
    class Program
    {
        static void Main(string[] args)
        {
            string str1 = null;
            string str2 = "привет";

            string result1 = str1 ?? string.Empty;
            string result2 = str2 ?? string.Empty;

            Console.WriteLine("Количество символов в строке: " + result1.Length);
            Console.WriteLine("Количество символов в строке: " + result2.Length);

            Console.ReadLine();
        }
    }
}