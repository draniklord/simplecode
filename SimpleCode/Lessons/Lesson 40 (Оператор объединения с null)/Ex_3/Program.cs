﻿using System;

namespace Ex_3
{
    class Program
    {
        static void Main(string[] args)
        {
            string str1 = null;
            string str2 = "привет";

            Console.WriteLine(str1 ?? "нет данных");
            Console.WriteLine(str2 ?? "нет данных");

            Console.ReadLine();
        }
    }
}