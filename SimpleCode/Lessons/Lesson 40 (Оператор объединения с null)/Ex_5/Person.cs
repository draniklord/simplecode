﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LessonFM
{
    class Person
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string SecondName { get; set; }

        public string GetFullName()
        {
            return $"Фамилия: {SecondName ?? "нет данных"} | Имя: {FirstName ?? "нет данных"} | Отчество: {MiddleName ?? "нет данных"}";
        }
    }
}