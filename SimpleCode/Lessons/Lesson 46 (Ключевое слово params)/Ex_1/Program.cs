﻿using System;

namespace Ex_1
{
    class Program
    {
        static int Sum(string msg, params int[] parametrs)
        {
            int result = 0;

            for (int i = 0; i < parametrs.Length; i++)
            {
                result += parametrs[i];
            }

            return result;
        }

        static void Main(string[] args)
        {
            int result = Sum("test", 5, 6, 5, 1, 7, 16);

            Console.WriteLine(result);

            Console.ReadLine();
        }
    }
}