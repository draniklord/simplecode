﻿using System;

namespace Ex_2
{
    class Program
    {
        static void Foo(params object[] parametrs)
        {
            string message = "Тип данных {0}, значение {1}";

            foreach (var item in parametrs)
            {
                Console.WriteLine(message, item.GetType(), item);
            }
        }

        static void Main(string[] args)
        {
            Foo("test", 5, 'q', 5.89f, true);

            Console.ReadLine();
        }
    }
}