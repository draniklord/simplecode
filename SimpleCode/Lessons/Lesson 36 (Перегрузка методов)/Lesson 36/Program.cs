﻿using System;

namespace Lesson_39
{
    /// <summary>
    /// Главный класс
    /// </summary>
    class Program
    {
        /// <summary>
        /// Возвращает сумму двух целых чисел
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        static int Sum(int a, int b)
        {
            return a + b;
        }

        static int Sum(int a, int b, int c)
        {
            return a + b + c;
        }

        static double Sum(double a, double b)
        {
            return a + b;
        }

        static void Main(string[] args)
        {
            int result = Sum(4, 5);
            int result2 = Sum(6, 6, 8);
            double result3 = Sum(3.4, 8.1);

            Console.WriteLine(result);
            Console.WriteLine(result2);
            Console.WriteLine(result3);

            Console.ReadLine();
        }
    }
}