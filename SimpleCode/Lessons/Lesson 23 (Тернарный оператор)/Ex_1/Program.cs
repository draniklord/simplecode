﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_1
{
    class Program
    {
        static void Main(string[] args)
        {
            bool accessAllowed;

            string storedPassword = "qwerty";
            string enteredPassword = Console.ReadLine();

            if (enteredPassword == storedPassword)
            {
                accessAllowed = true;
            }
            else
            {
                accessAllowed = false;
            }

            Console.WriteLine(accessAllowed);

            Console.ReadLine();
        }
    }
}