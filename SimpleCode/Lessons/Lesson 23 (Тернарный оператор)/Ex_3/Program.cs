﻿using System;

namespace Ex_3
{
    class Program
    {
        static void Main(string[] args)
        {
            int inputData = int.Parse(Console.ReadLine());

            int outputData = (inputData < 0) ? 0 : inputData;

            Console.WriteLine("\n" + outputData);

            Console.ReadLine();
        }
    }
}