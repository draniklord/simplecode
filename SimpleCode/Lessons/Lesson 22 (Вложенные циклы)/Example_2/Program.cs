﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example_2
{
    class Program
    {
        static void Main(string[] args)
        {
            var gamesInfo = GetGamesInfo();

            for (int i = 0; i < gamesInfo.Count; i++)
            {
                Console.WriteLine($"{gamesInfo[i].TypeOfGame}:");

                for (int j = 0; j < gamesInfo[i].Games.Count; j++)
                {
                    Console.WriteLine($"{gamesInfo[i].Games[j].Name}");
                }
                Console.WriteLine();
            }
        }

        static List<GameInfo> GetGamesInfo()
        {
            return new List<GameInfo>()
            {
                new GameInfo()
                {
                    TypeOfGame = GameType.RPG,
                    Games = new List<Games>()
                    {
                        new Game(){ Name = "Fallout 4" },
                        new Game() {Name = "Gothic 2" },
                        new Game() { Name = "The Elder Scrolls 3: Morrowind"}
                    }
                },
                new GameInfo ()
                {
                    TypeOfGame = GameType.Shooter,
                    Games = new List<Games>()
                    {
                        new Game(){ Name = "Counter Strike: Globale Offensive" },
                        new Game() {Name = "Battlefield 4" },
                        new Game() { Name = "Doom"}
                    }
                },

            }
        }
    }
}