﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 1; i <= 4; i++)
            {
                switch (i)
                {
                    case 1:
                        Console.WriteLine("Когда преподаватель дал курсову, студенты говорят: ");
                        for (int j = 0; j <= 5; j++)
                        {
                            Console.WriteLine("\t \"Ура\" ");
                        }
                        break;
                    case 2:
                        Console.WriteLine("Когда преподаватель отменил пару, студенты говорят: ");
                        for (int j = 0; j <= 5; j++)
                        {
                            Console.WriteLine("\t \"Ура\" ");
                        }
                        break;
                    case 3:
                        Console.WriteLine("Когда универ закрыли на карантин, студенты говорят: ");
                        for (int j = 0; j <= 5; j++)
                        {
                            Console.WriteLine("\t \"Хотим в универ!\" ");
                        }
                        break;

                    default:
                        Console.WriteLine("Да здравствует учеба!");
                        break;
                }
            }

            Console.ReadKey();
        }
    }
}