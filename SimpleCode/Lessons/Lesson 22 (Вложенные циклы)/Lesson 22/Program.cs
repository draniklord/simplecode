﻿using System;

namespace Lesson_22
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 1; i <= 3; i++)
            {
                Console.WriteLine("цикл №1 иттерация №: " + i);
                for (int j = 1; j <= 2; j++)
                {
                    Console.WriteLine("\tцикл №2 иттерация №: " + j);
                    for (int l = 1; l <= 3; l++)
                    {
                        Console.WriteLine("\t\tцикл №3 иттерация №: " + l);
                    }
                }
            }

            Console.ReadKey();
        }
    }
}