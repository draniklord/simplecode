﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите высоту: ");
            int height = int.Parse(Console.ReadLine());

            Console.Write("Введите длину: ");
            int width = int.Parse(Console.ReadLine());

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    Console.Write("#");
                }

                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}