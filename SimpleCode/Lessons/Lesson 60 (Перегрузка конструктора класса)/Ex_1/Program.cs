﻿using System;

// Перегрузка конструкторов класса

namespace Ex_1
{
    class Point
    {
        public Point()
        {
            _x = _y = 1;   
        }
        public Point(int x, int y)
        {
            _x = x;
            _y = y;
        }
        private int _x;
        private int _y;

        public void Print()
        {
            Console.WriteLine($"X: {_x}\tY: {_y}");
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            Point point1 = new Point();
            point1.Print();

            Point point2 = new Point(5, 8);
            point2.Print();
        }
    }
}