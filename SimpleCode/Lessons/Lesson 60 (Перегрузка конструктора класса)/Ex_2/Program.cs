﻿using System;

namespace Ex_2
{
    class Student
    {
        public Student(string lastName, DateTime birthday)
        {
            _lastName = lastName;
            _birthday = birthday;  
        }
        public Student(string lastName, string firstName, string middleName, DateTime birthday)
        {
            _lastName = lastName;
            _firstName = firstName;
            _middleName = middleName;
            _birthday = birthday;
        }
        public Student(Student student)
        {
            _firstName = student._firstName;
            _middleName = student._middleName; 
            _lastName=student._lastName;
            _birthday=student._birthday;
        }
        public void SetLastName(string lastName)
        {
            _lastName = lastName;
        }

        private string _lastName;
        private string _firstName;
        private string _middleName;
        private DateTime _birthday;

        public void Print()
        {
            Console.WriteLine($"Имя: {_firstName}\nФамилия: {_lastName}\nОтчество: {_middleName}\nДата рождение: {_birthday.ToString("MM.dd.yyyy")} г.\n");
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            Student student1 = new Student("Драник", "Руслан", "Сергеевич", new DateTime(2001, 5, 6));

            Student student2 = new Student(student1);

            student1.SetLastName("******");

            student1.Print();
            student2.Print();
        }
    }
}