﻿using System;

namespace Ex_2
{
    class MyClass
    {
        public byte Age;
        public string Name;

        public MyClass() { }
        // Создаем цепочку конструкторов
        public MyClass(string name) : this(name, 0)
        {
            Console.WriteLine("Поле Name");
        }

        public MyClass(byte age) : this("", age)
        {
            Console.WriteLine("Поле age");
        }

        // Главный конструктор
        public MyClass(string name, byte age)
        {
            if (age > 25)
                age = 25;

            Name = name;
            Age = age;
        }
    }

    class Program
    {
        static void Main()
        {
            MyClass obj = new MyClass(26);
            obj.Name = "Alexandr";

            Console.WriteLine("Имя - {0}; Возраст - {1}", obj.Name, obj.Age);
        }
    }
}