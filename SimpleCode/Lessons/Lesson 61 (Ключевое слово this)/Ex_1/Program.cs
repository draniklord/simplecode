﻿using System;

//Ключевое слово this

namespace Ex_1
{
    class Student
    {
        public Student(string lastName, DateTime birthday)
        {           
            this.lastName = lastName;
            this.birthday = birthday;
        }
        public Student(string lastName, string firstName, string middleName, DateTime birthday) : this(lastName, birthday)
        {
            //this.lastName = lastName;
            this.firstName = firstName;
            this.middleName = middleName;
            //this.birthday = birthday;
        }
        public Student(Student student)
        {
            firstName = student.firstName;
            middleName = student.middleName;
            lastName = student.lastName;
            birthday = student.birthday;
        }
        public void SetLastName(string lastName)
        {
            this.lastName = lastName;
        }

        private string lastName;
        private string firstName;
        private string middleName;
        private DateTime birthday;

        public void Print()
        {
            Console.WriteLine($"Имя: {firstName}\nФамилия: {lastName}\nОтчество: {middleName}\nДата рождение: {birthday.ToString("MM.dd.yyyy")} г.\n");
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            Student student1 = new Student("Драник", "Руслан", "Сергеевич", new DateTime(2001, 5, 6));

            student1.Print();
        }
    }
}