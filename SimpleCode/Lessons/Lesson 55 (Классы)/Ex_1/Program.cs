﻿using System;

// Классы

namespace Ex_1
{
    enum Color
    {
        Red,
        Green,
        Blue,
        Black,
        Orange
    }
    class Point
    {
        public int x;
        public int y; 
        
        public Color color;
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            Point point = new Point();

            //point = null;

            point.x = 4;
            point.y = 2;

            Console.WriteLine(point.x);
            Console.WriteLine(point.y);

            Point point2 = new Point();

            point2.x = 3;
            point2.y = 4;
            point2.color = Color.Red;

            Console.WriteLine($"X: {point2.x}   |   Y: {point2.y}   |   Color: {point2.color}");
        }
    }
}