﻿using System;
using Ex_2Test;

namespace Ex_2
{
    internal class Program
    {
        static Student GetStudent()
        {
            Student student = new Student();

            student.firstName = "Геныч";
            student.lastName = "Пукович";
            student.middleName = "Сюрикенович";
            student.age = 15;
            student.id = Guid.NewGuid();
            student.group = "ЭА-191";

            return student;
        }
        static void Print(Student student)
        {
            Console.WriteLine("Информация о студенте");
            Console.WriteLine($"ID {student.id}");
            Console.WriteLine($"Фамилия: {student.lastName}");
            Console.WriteLine($"Имя: {student.firstName}");
            Console.WriteLine($"Отчество: {student.middleName}");
            Console.WriteLine($"Возраст: {student.age}");
            Console.WriteLine($"Группа: {student.group}");
        }
        static void Main(string[] args)
        {
            var firtstStudent = GetStudent();

            Print(firtstStudent);
        }
    }
}