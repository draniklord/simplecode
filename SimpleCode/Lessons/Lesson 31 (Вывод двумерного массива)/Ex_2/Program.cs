﻿using System;

namespace Ex_2
{
    class Program
    {
        static void Main(string[] args)
        {

            int[,] myArray =
            {
                {16, 18, 55, 38, 1 },
                {1981, 23, 48, 653, 6 },
                {1, 8, 6, 4, 8 },
                {5651, 32165, 65132, 16512, 3 }
            };

            Console.WriteLine(myArray.Rank + "\n");//кол-во измерений 

            //int height = myArray.GetLength(0);//кол-во элементов в первом измерении (по вертикали)
            //int width = myArray.GetLength(1);

            for (int y = 0; y < myArray.GetLength(0); y++)
            {
                for (int x = 0; x < myArray.GetLength(1); x++)
                {
                    Console.Write(myArray[y,x] + "\t");
                }
                Console.WriteLine();
            }

            Console.ReadLine();
        }
    }
}