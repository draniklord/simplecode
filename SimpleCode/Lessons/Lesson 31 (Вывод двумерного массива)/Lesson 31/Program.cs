﻿using System;

namespace Lesson_31
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] myArray =
            {
                {16, 18, 55, 38, 1 },
                {1981, 23, 48, 653, 6 },
                {1, 8, 6, 4, 8 },
                {5651, 32165, 65132, 16512, 3 },
                {1, 2, 3, 4, 5 }
            };

            foreach (var item in myArray)
            {
                Console.Write(item + " ");
            }

            Console.ReadLine();
        }
    }
}