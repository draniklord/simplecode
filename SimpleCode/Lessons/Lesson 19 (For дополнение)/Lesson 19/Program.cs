﻿using System;

namespace Lesson_19
{
    class Program
    {
        static void Main(string[] args)
        {
            int limit = int.Parse(Console.ReadLine());

            for (int i = 0; i < limit; i++)
            {
                Console.WriteLine(i);
            }

            //for ( ; ; )
            //{
            //    Console.WriteLine("Nipka is so loh");
            //    System.Threading.Thread.Sleep(300);
            //    break;
            //}

            //int i = 0;
            //for (; i < 3; i++)
            //{
            //    Console.WriteLine("for_1: " + i);
            //}
            //for (; i < 5; i++)
            //{
            //    Console.WriteLine("for_2: " + i);
            //}

            Console.ReadLine();
        }
    }
}
