﻿using System;

namespace _6_В_обратном_порядке
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = int.Parse(Console.ReadLine());
            Console.WriteLine();

            for ( ; i >= 0; i--)
            {
                Console.WriteLine(i);
            }

            Console.ReadLine();
        }
    }
}