﻿using System;

namespace _6
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0, j = 5; i < 5; i++, j--)
            {
                Console.WriteLine("i: " + i);
                Console.WriteLine("j: " + j);
            }

            Console.ReadLine();
        }
    }
}