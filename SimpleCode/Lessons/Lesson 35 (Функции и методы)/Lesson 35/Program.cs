﻿using System;

namespace Lesson_35
{
    class Program
    {
        static int Sum(int value_1, int value_2)
        {
            int result = value_1 + value_2;

            return result;
        }

        static void Main(string[] args)
        {
            int a, b, c;

            a = int.Parse(Console.ReadLine());
            b = int.Parse(Console.ReadLine());

            c = Sum(a, b);

            PrintResult(c);

            Console.ReadLine();
        }

        static void PrintResult(int result)
        {
            Console.WriteLine("Результат сложения: " + result);
        }
    }
}