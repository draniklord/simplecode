﻿using System;

namespace ConsoleApplication1
{
    class MyMathOperation
    {
        public double r;
        public string s;

        // Возвращает площадь круга
        public double sqrCircle()
        {
            return Math.PI * r * r;
        }

        // Возвращает длину окружности
        public double longCircle()
        {
            return 2 * Math.PI * r;
        }

        public void writeResult()
        {
            Console.WriteLine("Вычислить площадь или длину? s/l:");
            s = Console.ReadLine();
            s = s.ToLower();
            if (s == "s")
            {
                Console.WriteLine("Площадь круга равна {0:#.###}", sqrCircle());
                return;
            }
            else if (s == "l")
            {
                Console.WriteLine("Длина окружности равна {0:#.##}", longCircle());
                return;
            }
            else
            {
                Console.WriteLine("Вы ввели не тот символ");
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите радиус: ");
            string radius = Console.ReadLine();

            MyMathOperation newOperation = new MyMathOperation { r = double.Parse(radius) };
            newOperation.writeResult();

            Console.ReadLine();
        }
    }
}