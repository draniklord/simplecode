﻿using System;

namespace Ex_2
{
    class Program
    {
        static void PrintLine()
        {
            Console.WriteLine("Метод PrintLine был вызван");
        }

        static void Main(string[] args)
        {
            PrintLine();

            Console.ReadLine();
        }
    }
}