﻿using System;

// Конструкторы

namespace Ex_1
{
    class Gun
    {
        /// <summary>
        /// Орудие заряжено?
        /// </summary>
        /// <param name="isLoaded"></param>
        public Gun(bool isLoaded)
        {
            //IsLoaded = true;
            _isLoaded = isLoaded;
        }
        private bool _isLoaded;

        private void Reload()
        {
            Console.WriteLine("Заряжаю...");

            _isLoaded = true;

            Console.WriteLine("Заряжено!");
        }
        public void Shoot()
        {
            if (!_isLoaded)
            {
                Console.WriteLine("Орудие не заряжено!");
                Reload();
            }
            Console.WriteLine("Пиф-Паф\n");
            _isLoaded = false;
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            Gun gun = new Gun(isLoaded:true);
            gun.Shoot();
            gun.Shoot();
        }
    }
}