﻿using System;

namespace Ex_2
{
    class Point
    {
        public Point(int x, int y)
        {
            _x = x;
            _y = y;
        }
        private int _x;
        private int _y;

        public void Print()
        {
            Console.WriteLine($"X: {_x}\tY: {_y}");
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            Point point = new Point(5, 6);
            point.Print();
        }
    }
}