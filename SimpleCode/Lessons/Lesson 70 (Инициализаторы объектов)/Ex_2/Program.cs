﻿namespace Ex_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Person person1 = new Person();
            person1.FirstName = "Иван";
            person1.LastName = "Альтаир";

            Address address = new Address();
            address.City = "Белогорье";
            address.Country = "Федерация";
            address.Region = "Разумное";

            person1.Address = address;

            Person person2 = new Person
            {
                FirstName = "Олег",
                LastName = "Апальков",
                Address = new Address
                {
                    City = "17",
                    Country = "Заполярье",
                    Region = "Петровский"
                }
            };

            Person person3 = new Person("Кирилл", "Котуманов")
            {
                Address = new Address
                {
                    City = "17",
                    Country = "Заполярье",
                    Region = "Петровский"
                }
            };
        }
    }
}