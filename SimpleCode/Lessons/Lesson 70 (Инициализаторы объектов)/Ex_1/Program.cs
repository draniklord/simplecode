﻿// Инициализация объектов

namespace Ex_1
{
    class Cat
    {
        public int Age { get; set; }
        public string Name { get; set; }

        public Cat()
        {

        }

        public Cat(string name)
        {
            Name = name;    
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            Cat cat1 = new Cat();
            cat1.Age = 3;
            cat1.Name = "Кицык";

            Cat cat2 = new Cat
            {
                Age = 3,
                Name = "Бусик"
            };

            Cat cat3 = new Cat("Барсук");
            cat3.Age = 3;   
        }
    }
}