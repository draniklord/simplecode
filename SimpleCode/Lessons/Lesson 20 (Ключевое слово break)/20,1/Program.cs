﻿using System;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            // В данном цикле выведутся числа от 1 до 5 вместо 100
            for (int i = 1; i < 100; i++)
                if (i <= 5)
                {
                    Console.WriteLine(i);
                }
                else
                {
                    break;
                }                

            Console.ReadLine();
        }
    }
}