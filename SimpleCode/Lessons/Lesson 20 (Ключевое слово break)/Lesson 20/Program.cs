﻿using System;

namespace Lesson_20
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; i < 100; i++)
            {
                string msg = Console.ReadLine();

                if (msg == "exit")
                {
                    break;
                }

                Console.WriteLine(i);
            }

            Console.ReadLine();
        }
    }
}