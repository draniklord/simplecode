﻿using System;

// Статический класс

namespace Ex_1
{
    static class MyClass
    {
        public static void Foo()
        {
            Console.WriteLine("Foo");
        }

        public static void Bar()
        {
            Console.WriteLine("Bar");
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            MyClass.Foo();
            MyClass.Bar();
        }
    }
}