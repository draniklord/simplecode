﻿using System;

namespace Lesson_12
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 5;
            int b = 4;
           
            Console.WriteLine(a == b); //False
            Console.WriteLine(a != b); //True
            Console.WriteLine(a > b); //True
            Console.WriteLine(a < b); //False
            Console.WriteLine(a >= b); //True
            Console.WriteLine(a <= b); //False

            Console.ReadKey();
        }
    }
}