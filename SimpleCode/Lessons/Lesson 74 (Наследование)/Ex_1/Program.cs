﻿using System;

// модификатор protected

namespace Ex_1
{
    public class A
    {
        public int publicField = 0;
        private int privateField = 1;
        protected int protectedField = 2;

        public A()
        {
            Console.WriteLine(publicField); //поле доступно
            Console.WriteLine(privateField); //поле доступно
            Console.WriteLine(protectedField); //поле доступно 
        }
        public void Foo()
        {
            Console.WriteLine(publicField); //поле доступно
            Console.WriteLine(privateField); //поле доступно
            Console.WriteLine(protectedField); //поле доступно 
        }
    }

    class B : A
    {
        public B()
        {
            Console.WriteLine(publicField); //поле доступно
            //Console.WriteLine(privateField); //поле недоступно
            Console.WriteLine(protectedField); //поле доступно 

            Foo();
        }

        public void Bar()
        {
            Foo();
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            A a = new A();

            Console.WriteLine(a.publicField); //поле доступно
            //Console.WriteLine(a.privateField); //поле недоступно
            //Console.WriteLine(a.protectedField); //поле недостуно

            B b = new B();

            Console.WriteLine(b.publicField); //поле доступно
            //Console.WriteLine(b.privateField); //поле недоступно
            //Console.WriteLine(b.protectedField); //поле недостуно

            b.Foo();
        }
    }
}