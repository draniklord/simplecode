﻿using System;

namespace Lesson_15
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.Clear();

                int a = int.Parse(Console.ReadLine());

                switch (a)
                {
                    case 1:
                        Console.WriteLine("Вы ввели число 1");
                        break;
                    case 2:
                        {
                            Console.WriteLine("Вы ввели число 2");
                        }
                        break;
                    case 3:
                    case 4:
                        Console.WriteLine("Вы ввели число 3 или 4");
                        break;
                    default:
                        Console.WriteLine("Ты че ввел, дурак?");
                        break;
                }

                string b = Console.ReadLine();

                switch (b)
                {
                    case "-":
                        Console.WriteLine("Вы ввели знак минус");
                        break;
                    case "+":
                        Console.WriteLine("Вы ввели знак плюс");
                        break;
                    default:
                        Console.WriteLine("Ты че ввел, дурак?");
                        break;
                }

                Console.ReadKey();
            }
        }
    }
}