﻿using System;

namespace Lesson_41
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = null;

            str ??= string.Empty;

            Console.WriteLine("Количество символов в строке: " + str.Length);

            Console.ReadLine();
        }
    }
}