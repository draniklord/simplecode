﻿using System;

namespace Ex_4
{
    class Program
    {
        static int b = 11;

        static void Foo()
        {
            int b = 9;

            Console.WriteLine(b);
        }

        static void Main(string[] args)
        {
            Foo();

            Console.ReadLine();
        }
    }
}