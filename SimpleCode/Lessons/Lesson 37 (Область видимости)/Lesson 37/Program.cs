﻿using System;

namespace Ex_1
{
    class MyClass
    {
        a++; //ошибка, т.к в другом классе
    }

    class Program
    {
        int a = 11;

        static void Foo()
        {
            int b = 9;
        }
        static void Main(string[] args)
        {
            //b++; //такой переменной нет

            //a++; //ошибка, т.к метод static

            //чтобы воспользоваться переменной а
            Program program = new Program();
            program.a;

            Console.ReadLine();
        }
    }
}