﻿using System;

namespace Ex_2
{
    class Program
    {
        static void Main(string[] args)
        {
            // тип_данных [,] имя_массива
            int[,] myArray = new int[3, 5];

            myArray[0, 2] = 99;

            Console.WriteLine(myArray[0, 2]);

            Console.ReadLine();
        }
    }
}