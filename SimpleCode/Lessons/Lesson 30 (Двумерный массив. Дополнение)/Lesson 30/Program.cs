﻿using System;

namespace Lesson_30
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] myArray = new int[3, 5] 
            {
                {16, 18, 55, 38, 1 },
                {1981, 23, 48, 653, 6 },
                {1, 8, 6, 4, 8 }
            };

            Console.WriteLine(myArray[0,2]);

            Console.ReadLine();
        }
    }
}