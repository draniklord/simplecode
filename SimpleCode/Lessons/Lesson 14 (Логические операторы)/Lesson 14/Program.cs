﻿using System;

namespace Lesson_14
{
    class Program
    {
        public static bool GetTemperature()
        {
            int temperature = 70;
            bool isHighTemperature = temperature >= 60;
            return isHighTemperature;
        }

        public static bool GetCoolingStatus()
        {
            int fanSpeed = 5000;
            bool hasNoCooling = fanSpeed <= 1000;
            return hasNoCooling;
        }

        static void Main(string[] args)
        {
            if (GetTemperature() && GetCoolingStatus())
            {
                Console.WriteLine("Угроза повреждения процессора");
            }
            else
            {
                Console.WriteLine("Температура четкая");
            }

            Console.ReadLine();
        } 
    }
}