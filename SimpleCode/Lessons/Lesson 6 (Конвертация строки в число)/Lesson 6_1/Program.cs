﻿using System;

namespace Lesson_6_1
{
    class Program
    {
        static void Main(string[] args)
        {
            string str;
            int a, b, result;

            Console.WriteLine("Введите первое число");

            str = Console.ReadLine();
            a = Convert.ToInt32(str);

            Console.WriteLine("Введите второе число");

            str = Console.ReadLine();
            b = Convert.ToInt32(str);

            result = a + b;

            Console.WriteLine("Сумма чисел равно " + result);

            Console.ReadLine();
        }
    }
}