﻿using System;

namespace Lesson_7
{
    class Program
    {
        static void Main(string[] args)
        {
            //string str = Console.ReadLine();
            int a = int.Parse(Console.ReadLine());

            bool result = int.TryParse(Console.ReadLine(), out a);

            if (result)
            {
                Console.WriteLine("Конвертация прошла успешно " + a);
            }
            else
            {
                Console.WriteLine("Ошибка при конвертации");
            }

            Console.ReadLine();
        }
    }
}