﻿using System;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Выводим числа кратные 5
            for (byte i = 1; i <= 100; i++)
            {
                if (i % 5 != 0) continue;
                Console.Write("\t{0}", i);
            }

            Console.ReadLine();
        }
    }
}