﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ_2
{
    class Program
    {
        static void Main(string[] args)
        {
            double a;
            a = int.Parse(Console.ReadLine());

            if (0 == a % 2)
            {
                Console.WriteLine("Четное число");
            }
            else
            {
                Console.WriteLine("Нечетное число");
            }

            Console.ReadLine();
        }
    }
}
