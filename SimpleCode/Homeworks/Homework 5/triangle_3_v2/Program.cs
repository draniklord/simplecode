﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace triangle_3_v2
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.Write("Введите высоту треугольника: ");
            int height = int.Parse(Console.ReadLine());

            for (int i = 0; i < height; i++)
            {
                Console.SetCursorPosition(height - i, i + 1);
                for (int j = 0; j <= i; j++)
                {
                    Console.Write("*");
                }
            }

            Console.ReadKey();
        }
    }
}
