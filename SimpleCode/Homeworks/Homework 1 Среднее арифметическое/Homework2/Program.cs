﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework2
{
    class Program
    {
        static void Main(string[] args)
        {
            // конвертер валют с фиксированной ставкой 

            double RubToUsd = 72.80; 
            double RubToEur = 86.37;
            double Rub;

            Console.WriteLine("Введите сумму в рублях");

            Rub = double.Parse(Console.ReadLine());


            Console.WriteLine(Rub + " RUB в USD = " + Rub / RubToUsd);
            Console.WriteLine(Rub + " RUB в EUR = " + Rub / RubToEur);


            Console.ReadLine();

        }
    }
}
