﻿using System;

namespace Ex_1
{
    class Program
    {
        static int SumArray(int[] myArray, int result = 0, int i = 0)
        {
            if (i < myArray.Length)
            {
                result = result + myArray[i];

                SumArray(myArray, result, i + 1);
            }

            return result + myArray[i];
        }

        static void Main(string[] args)
        {
            int[] myArray = { 23, 46, 256, 2 };

            int result = SumArray(myArray);

            Console.WriteLine(result);
        }
    }
}