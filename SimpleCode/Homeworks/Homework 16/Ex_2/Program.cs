﻿using System;

namespace Ex_2
{
    class Program
    {
        static int SumArray(int[] myArray, int i = 0)
        {
            if (i >= myArray.Length)
            {
                return 0;
            }

            int result = SumArray(myArray, i + 1);

            return result + myArray[i];
        }

        static void Main(string[] args)
        {
            int[] myArray = { 5, 124, 533, 23 };

            int result = SumArray(myArray);

            Console.WriteLine(result);       
        }
    }
}