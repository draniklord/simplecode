﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_4
{
    class Program
    {
        // минимальный элемент массива
        static void Main(string[] args)
        {
            Console.Write("Введите количество элементов в массиве: ");
            int n = int.Parse(Console.ReadLine());

            Console.WriteLine($"\nВведите {n} элементов массива");
            int[] myArray = new int[n];

            for (int i = 0; i < myArray.Length; i++)
            {
                Console.Write($"   Элемент массива №{i + 1}: ");
                myArray[i] = int.Parse(Console.ReadLine());
            }

            int minValue = myArray[0];

            for (int i = 1; i < myArray.Length; i++)
            {
                if (myArray[i] < minValue)
                {
                    minValue = myArray[i];
                }
            }

            Console.WriteLine("\nМинимальное число массива: " + minValue);

            Console.ReadLine();
        }
    }
}