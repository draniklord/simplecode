﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework_6
{
    class Program
    {
        // Заполнение массива с клавиатуры

        static void Main(string[] args)
        {
            Console.Write("Введите количество элементов в массиве: ");
            uint n = uint.Parse(Console.ReadLine());

            Console.WriteLine($"\nВведите {n} элементов массива");
            uint[] KeyboardArray = new uint[n];

            for (int i = 0; i < KeyboardArray.Length; i++)
            {
                Console.Write($"   Элемент массива №{i + 1}: ");
                KeyboardArray[i] = uint.Parse(Console.ReadLine());           
            }

            Console.WriteLine("\nВывод массива: ");

            for (int i = 0; i < KeyboardArray.Length; i++)
            {
                Console.Write(KeyboardArray[i] + "\t");
            }

            //Console.WriteLine("\nВывод массива: ");

            //for (uint i = n; i < 0; --i)
            //{
            //    Console.Write(KeyboardArray[i] + "\t");
            //}

            Console.ReadLine();
        }
    }
}