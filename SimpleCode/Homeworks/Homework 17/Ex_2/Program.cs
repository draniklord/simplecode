﻿using System;

namespace Ex_2
{
    internal class Program
    {
        static int Sum(int value)
        {
            int result = 0;

            while (value > 0)
            {
                result+= value % 10;
                value = value / 10;
            }

            return result;
        }

        static void Main(string[] args)
        {
            int value = 561;

            int result = Sum(value);
            Console.WriteLine(result);
        }
    }
}
