﻿using System;

namespace Ex_1
{

    /* Найти сумму цифр чиcла    561 = 12 */

    internal class Program
    {
        static int Sum(int value)
        {
            if (value < 10)
                return value;

            int digit = value % 10;

            int nextValue = value / 10;

            return digit + Sum(nextValue);
        }

        static void Main(string[] args)
        {
            int value = 561;

            int result = Sum(value);

            Console.WriteLine(result);
        }
    }
}