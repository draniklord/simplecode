﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework_10_11
{
    class Program
    {
        static void WriteLine(string symbol, uint symbolCount )
        {
            for (int i = 0; i < symbolCount; i++)
            {
                Console.Write(symbol + "\t");
            }
        }

        static void Main(string[] args)
        {
            //WriteLine();

            Console.Write("Введите символ: ");
            string symbol = Console.ReadLine();

            Console.Write("Введите количество символов: ");
            uint symbolCount = uint.Parse(Console.ReadLine());

            WriteLine(symbol, symbolCount);

            Console.ReadLine();
        }
    }
}