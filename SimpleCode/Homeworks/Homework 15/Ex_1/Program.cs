﻿using System;

namespace Ex_1
{
    class Program
    {
        static void Array(int[] myArray, int i = 0)
        {
            if (myArray.Length > i)
            {
                Console.WriteLine(myArray[i]);
                Array(myArray, i + 1);
            }
        }

        static void Main(string[] args)
        {
            int[] myArray = { 5, 13, 57, 8 };

            Array(myArray);
        }
    }
}