﻿using System;

namespace Homework12
{
    class Program
    {
        static void Resize(int n, ref int[] arr)
        {
            arr = new int[n];
        }

        static void Main(string[] args)
        {
            Console.Write("Введите количество элементов массива: ");
            int n = int.Parse(Console.ReadLine());

            int[] myArray = new int[n];

            Console.WriteLine();
            Console.Write("Вы создали массив с количество элементов: " + myArray.Length);

            Console.WriteLine("\nХотите изменить длину массива?(Да/Нет)");
            string str = Console.ReadLine();

            while (true)
            {
                if (str == "Да" || str == "да")
                {
                    Console.Write(str);
                    //Resize(n, ref myArray);
                    break;
                }
                else if (str == "Нет" || str == "нет")
                {
                    Console.WriteLine("\nЗавершение работы!");
                    break;
                }
                else
                {
                    Console.WriteLine("Данный вариант ответа отсутствует!");
                }
                Console.WriteLine("\nХотите изменить длину массива?(Да/Нет)");
                str = Console.ReadLine();
            }




            //Console.WriteLine(myArray.Length);

            Console.ReadLine();
        }
    }
}
