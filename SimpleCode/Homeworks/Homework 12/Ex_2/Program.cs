﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_2
{
    class Program
    {
        static void Resize<T>(ref T[] array, int newSize) // "<>" позволяет работать с любым типом данных
        {
            T[] newArray = new T[newSize];

            for (int i = 0; i < array.Length && i < newArray.Length; i++)
            {
                newArray[i] = array[i];
            }

            array = newArray;
        }

        static void Main(string[] args)
        {
            int[] myArray = { 1, 4, 6 };

            Resize(ref myArray, 2);

            for (int i = 0; i < myArray.Length; i++)
            {
                Console.Write(myArray[i] + "\t");
            }

            Console.ReadLine();
        }
    }
}