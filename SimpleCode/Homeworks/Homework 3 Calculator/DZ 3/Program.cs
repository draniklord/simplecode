﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ_3
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.Clear();

                double a, b;
                string result;

                try
                {
                   Console.WriteLine("Введите первое число");
                a = double.Parse(Console.ReadLine());

                Console.WriteLine("Выберите действие: '+' '-' '*' '/'");
                result = Console.ReadLine();

                Console.WriteLine("Введите второе число");
                b = double.Parse(Console.ReadLine());
                }
                catch (Exception)
                {
                    Console.WriteLine("Ошибочка, друг");
                    Console.ReadLine();
                    continue;
                }

                switch (result)
                {
                    case "+":
                        Console.WriteLine(a + b);
                        break;

                    case "-":
                        Console.WriteLine(a - b);
                        break;

                    case "*":
                        Console.WriteLine(a * b);
                        break;

                    case "/":
                        if (b == 0)
                        {
                            Console.WriteLine("Бесконечность");
                        }
                        else
                        {
                            Console.WriteLine(a / b);
                        }
                        break;

                    default:
                        Console.WriteLine("Ошибка! Неизвестное действие");
                        break;
                }

                Console.ReadLine();
            }
        }
    }
}
