﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace If_Else
{
    class Program
    {
        static void Main(string[] args)
        {
            double a, b;
            string result;

            Console.WriteLine("Введите первое число");
            a = double.Parse(Console.ReadLine());

            Console.WriteLine("Выберите действие: '+' '-' '*' '/'");
            result = Console.ReadLine();

            Console.WriteLine("Введите второе число");
            b = double.Parse(Console.ReadLine());


            if (result == "+")
            {
                Console.WriteLine(a + b);
            }
            else if (result == "-")
            {
                Console.WriteLine(a - b);
            }
            else if (result == "*")
            {
                Console.WriteLine(a * b);
            }
            else if (result == "/")
            {
                if (b == 0)
                {
                    Console.WriteLine("Бесконечность");
                }
                else
                {
                    Console.WriteLine(a / b);
                }
            }
            else
                Console.WriteLine("Ошибка! Неизвестное действие");


            Console.ReadLine();
        }
    }
}
